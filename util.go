// Copyright 2019 Andrew Apted.
// This code is under the GNU General Public License, version 3
// or (at your option) any later version.

/*

Utility functions

*/

package main

import "fmt"
import "math/rand"

func Abs(i int) int {
	if i < 0 {
		return -i
	} else {
		return i
	}
}

func Min(a, b int) int {
	if a < b {
		return a
	} else {
		return b
	}
}

func Max(a, b int) int {
	if a > b {
		return a
	} else {
		return b
	}
}

//----------------------------------------------------------------------

var id_set map[string]int

func ClearIds() {
	id_set = make(map[string]int)
}

func AllocId(prefix string) string {
	suffix := id_set[prefix] + 1

	id_set[prefix] = suffix

	return fmt.Sprintf("%s%02d", prefix, suffix)
}

//----------------------------------------------------------------------

type Direction int

const (
	DN Direction = 0
	LF Direction = 1
	UP Direction = 2
	RT Direction = 3

	NUM_DIRS = 4
)

func (dir Direction) String() string {
	switch dir {
	case DN:
		return "DN"
	case LF:
		return "LF"
	case UP:
		return "UP"
	case RT:
		return "RT"
	default:
		return "??"
	}
}

func (dir Direction) Clockwise() Direction {
	return (dir + 1) & 3
}

func (dir Direction) AntiClock() Direction {
	return (dir + 3) & 3
}

func (dir Direction) Opposite() Direction {
	return dir ^ 2
}

func (dir Direction) IsHoriz() bool {
	return dir == LF || dir == RT
}

func (dir Direction) IsVert() bool {
	return dir == DN || dir == UP
}

func (dir Direction) Transpose() Direction {
	return dir ^ 1
}

func (dir Direction) MirrorX() Direction {
	if dir == LF || dir == RT {
		return dir.Opposite()
	} else {
		return dir
	}
}

func (dir Direction) MirrorY() Direction {
	if dir == DN || dir == UP {
		return dir.Opposite()
	} else {
		return dir
	}
}

func Nudge(x, y int, dir Direction) (int, int) {
	switch dir {
	case DN:
		return x, y - 1
	case UP:
		return x, y + 1
	case LF:
		return x - 1, y
	case RT:
		return x + 1, y
	default:
		panic("bad dir for nudge()")
	}
}

//----------------------------------------------------------------------

type BBox struct {
	Valid  bool
	X1, Y1 int
	X2, Y2 int
}

func (b *BBox) Clear() {
	b.Valid = false
}

func (b *BBox) AddPoint(x, y int) {
	if !b.Valid {
		b.Valid = true
		b.X1, b.X2 = x, x
		b.Y1, b.Y2 = y, y
		return
	}

	b.X1 = Min(b.X1, x)
	b.Y1 = Min(b.Y1, y)
	b.X2 = Max(b.X2, x)
	b.Y2 = Max(b.Y2, y)
}

func (b *BBox) AddRect(x1, y1, x2, y2 int) {
	if !b.Valid {
		b.Valid = true
		b.X1, b.X2 = x1, x2
		b.Y1, b.Y2 = y1, y2
		return
	}

	b.X1 = Min(b.X1, x1)
	b.Y1 = Min(b.Y1, y1)
	b.X2 = Max(b.X2, x2)
	b.Y2 = Max(b.Y2, y2)
}

func (b *BBox) String() string {
	if b.Valid {
		return fmt.Sprintf("[%d..%d, %d..%d]", b.X1, b.X2, b.Y1, b.Y2)
	} else {
		return "[empty]"
	}
}

//----------------------------------------------------------------------

// Rand produces a value between 0 and modulo-1.
func Rand(modulo int) int {
	return rand.Int() % modulo
}

// Between produces a random value in the inclusive range,
// i.e. results will be >= low and <= high.
func Between(low, high int) int {
	modulo := high - low + 1

	if modulo <= 1 {
		return low
	} else {
		return low + rand.Int()%modulo
	}
}

// Skew produces a random value that is generally near the
// 'mid' value 'mid', but can be as far away as the 'dist' value.
// Result is always between (mid - dist) and (mid + dist).
func Skew(mid, dist int) int {
	A := Rand(dist + 1)
	B := Rand(dist + 1)

	return mid + A - B
}

// Odds produces a true or false value, where the probability
// of true depends on the 'perc' parameter (a percentage).
// When perc < 0 the result is always false, and when > 100
// the result is always true.
func Odds(perc float64) bool {
	if perc <= 0 {
		return false
	} else {
		return rand.Float64() < perc/100
	}
}

func RandSel(perc float64, a, b int) int {
	if Odds(perc) {
		return a
	} else {
		return b
	}
}

// IndexByProbs takes an array of probabilities, and returns an
// index value (0 to len-1) based on those probabilities, where
// higher values a more likely than lower values.  Zero values
// *are* allowed, but not negative.  The array must not be empty.
func IndexByProbs(probs []float64) int {
	if len(probs) == 0 {
		panic("IndexByProbs: empty table")
	}

	var total float64
	for _, val := range probs {
		total += val
	}

	if total > 0 {
		target := rand.Float64() * total

		for idx, val := range probs {
			if target < val {
				return idx
			}
			target -= val
		}
	}

	return 0
}
