
THE POPULATOR
=============

by Andrew Apted, 2019.


Introduction
------------

This program is a randomizer for classic DOOM levels, i.e. it
replaces the monsters and weapons in the level with new ones
chosen at random, giving you a different experience each time.
Unlike other randomizer mods, The Populator simulates the
battles with the chosen monsters and gives you enough health
and ammunition to complete the level.  You can also customize
the results by editing some fairly simple text files.


Website
-------

https://gitlab.com/andwj/populate


Binary Packages
---------------

https://gitlab.com/andwj/populate/tags/v0.666

Each package contains the executable (EXE), a text document,
and a folder called "doom2" with several game definition files.
Please make sure all these files get unpacked properly.


Legalese
--------

The Populator is Copyright &copy; 2019 Andrew Apted.

The Populator is Free Software, under the terms of the GNU General
Public License, version 3 or (at your option) any later version.
See the [LICENSE.txt](LICENSE.txt) file for the complete text.

The Populator comes with NO WARRANTY of any kind, express or
implied.  Your use of this program is at YOUR OWN RISK.
Please read the license for full details.


Compiling
---------

This program is written in pure Go, and the dependencies are
quite minimal (and are also pure Go).  CGO is not required,
making compiling and even cross-compiling fairly easy.

To build and install the binary:
```
go get -v gitlab.com/andwj/populate
```


Running
-------

The Populator is a command-line program, which means you
normally run it from a terminal or shell window, however you
can also just drag a WAD file onto the executable and that
should work -- a new WAD file will be generated (this has been
tested in Windows, I don't know if it works in MacOS or Linux).

By default, the filename of the new WAD file will be the old
filename with "_pop" appended to it (before the extension).
A different output filename can be specified with the `--output`
option.  For a full list of supported options, run the program
with the `--help` option.

Currently the only supported game is DOOM II, as well as the
spin-offs TNT Evilution and The Plutonia Experiment.  Running
this on wads for the original DOOM is not recommended, as it
will add monsters and weapons into the levels which that game
does not support (if you are very keen to do this, then you
could edit the config files to disable the DOOM2 stuff -- see
the section on customization below).


Limitations
-----------

Boss maps, such as MAP07 and MAP30 of DOOM II, are not changed.
That's because boss maps usually have special requirements, e.g.
certain monsters should exist and their death triggers something
(like the exit door to open).

Certain items are currently left unchanged, including all of the
powerups, as well as armor and the big-ticket health items like
the soulsphere and megasphere.  I'm not really sure yet whether
randomizing or removing some of these is a good idea.

Secret areas are not treated any differently from normal areas
of a map, so large health items (even weapons) will be assumed
to be found.  If you find yourself wondering where all the
health is, then there's a good chance you've missed a secret
somewhere along the way.

There is a lot of code dedicated to figuring out the progression
of a map, and places where the player is and isn't meant to go.
However DOOM maps are tricky to analyse, as their structure can
change during play, so the analysis code often gets it wrong.
The biggest symptom of this is having health and ammo in places
far away from where you actually need it.


Customizing
-----------

Inside the `doom2` folder are four definition files which control
many aspects on how levels are handled.  The files are:

- `Game.ini` has miscellaneous info about the game
- `Monsters.ini` defines each type of monster
- `Weapons.ini` defines each kind of weapon
- `Pickups.ini` defines the various pickup items

These files are text files and should be edited with a text editor
such as NOTEPAD in Windows or gedit in Linux.  Do *NOT* use a word
processor program, such as Microsoft Word, because they do not
create plain text files (and what they create cannot be handled).

In these files, a semicolon ';' begins a comment line which will
be ignored.  Lines with square brackets like '[IMP]' begin a new
entry.  Other lines contain a keyword followed by '=' and a value,
and these assign a certain property to the current entry.  The
comments at the top of each file give a brief description of all
the accepted keywords.

The following fields are the most useful to change:

`prob` is a probability of adding that monster, weapon or item.
Higher values make the monster or item more likely to be used.
Set this to zero to completely disable a monster or item.
Note this is *NOT* a percentage or absolute value, so values
above 100 are perfectly valid.

`skip_prob` is the chance of skipping a monster for a whole level.
It can only be used with monsters, it does not work for weapons
or pickup items.  This value *IS* a percentage, so 100 means the
monster is always skipped (never added).  The default is zero.

`appear` controls how soon a monster or weapon is introduced in
an episode.  The value ranges from 1 to 9, where 1 is the very
start and 9 is close to the end.  Setting all appear values to 1
will disable the ramped introduction of monsters, allowing any
monsters to appear in any map (and similarly for weapons).

`usage` is only used with weapons, and is a measure of how much
the weapon will get used by a player (relative to other weapons).
This value mainly affects the types of ammo produced for a map,
e.g. increasing `usage` for the plasma rifle will generally
provide more cells, decreasing `usage` for the rocket launcher
will generally create less rocket ammo.

Changing the other fields is not recommended.

