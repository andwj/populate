// Copyright 2019 Andrew Apted.
// This code is under the GNU General Public License, version 3
// or (at your option) any later version.

/*

Planning, especially over the course of a megawad.

*/

package main

//import "fmt"

type Planning struct {
	mon_along  float64 // monster 'appear' must be <= this (or so)
	item_along float64 // weapon 'appear' must be <= this
}

func Planner(maps []*MapInfo) {
	for i, m := range maps {
		ep, along := CalcEpisodeAlong(maps, i)

		m.mon_along = 1.1 + ep*2.0 + along*9.0
		m.item_along = 1.2 + ep*1.0 + along*9.0

		/*
			fmt.Printf("%s : mon <= %1.2f  item <= %1.2f\n", m.name, m.mon_along, m.item_along)
		*/
	}
}

func CalcEpisodeAlong(maps []*MapInfo, idx int) (ep float64, along float64) {
	m := maps[idx]

	if len(maps) == 1 {
		ep = 0
		along = 1.0

	} else if len(maps) < 5 {
		ep = 0
		along = float64(idx) / float64(len(maps)-1)

	} else if len(m.name) == 4 && m.name[0] == 'E' && m.name[2] == 'M' {
		ep = float64(m.name[1] - '1')
		along = float64(m.name[3]-'1') / 6.0

	} else {
		ep = float64(idx / 11)
		along = float64(idx%11) / 10.0
	}

	if ep < 0 {
		ep = 0
	} else if ep > 4 {
		ep = 4
	}

	if along < 0 {
		along = 0
	} else if along > 1 {
		along = 1
	}

	return
}
