// Copyright 2019 Andrew Apted.
// This code is under the GNU General Public License, version 3
// or (at your option) any later version.

/*

Analyse WAD levels.

Determine where the player can traverse in the level, and
ideally the visit order of the spaces.

*/

package main

import "fmt"

import "gitlab.com/andwj/wad"

type Analysis struct {
	things  []*AnThing
	lines   []*AnLine
	sectors []*AnSector

	clusters []*Cluster

	// tip of the tree (acyclic directed graph), and exit point
	start_cluster *Cluster
	exit_cluster  *Cluster
}

// AnThing is basically an extension of wad.Thing containing some
// extra information (the results of the analysis).
type AnThing struct {
	raw *wad.Thing

	def    *EntityDef
	sector *AnSector

	// if this is true, thing won't exist in the written level
	discard bool
}

// AnSector is basically an extension of wad.Sector containing some
// extra information (the results of the analysis).
type AnSector struct {
	raw *wad.Sector

	damaging bool

	tele_dests   *SectorGroup
	door_dests   *SectorGroup
	locked_dests *SectorGroup

	lift_dests   *SectorGroup
	bridge_dests *SectorGroup
	mover_dests  *SectorGroup

	area *Area
}

// AnLine is basically an extension of wad.Line containing some
// extra information (the results of the analysis).  Unlike the
// wad structure, we don't use anything like a "SideDef".
type AnLine struct {
	raw *wad.LineDef

	kind string // from LineSpecialKind()

	front *AnSector
	back  *AnSector
}

// Cluster is a group of Areas which are contiguous for travel
// by the player (for the normal kinds).
//
// Clusters are also nodes in a graph, and connect to each other
// via doors, moving floors, teleporters, direct connnection, or
// at worst some arbitrary connection (to cope with situations
// where the map analysis fails).
//
// Other types of clusters handle monster closets/cages and
// "voodoo doll" areas.
type Cluster struct {
	kind     string // either "NORMAL", "CLOSET" or "VOODOO"
	sub_kind string // reflects method used for detection
	name     string // for debugging

	areas      []*Area
	out_links  []*Link

	// initially each cluster has a unique "link_set" value.
	// after linking two clusters, they will share a value.
	// after linking everything, only a single value remains.
	link_set *Cluster

	// the monsters in this cluster
	bunches []*Bunch
	battled bool

	// existing items in this cluster (armor, big health, weapons)
	items   []*PickupDef
	weapons []*WeaponDef

	mon_spots  SpotList
	item_spots SpotList
	big_spots  SpotList
}

// Area is a group of directly connected sectors which have the
// same floor height, and ceiling heights high enough for the
// player to fit.  Each Area gets rendered by the spots code
// to produce monster and item spots.
type Area struct {
	name string // for debugging

	cluster *Cluster

	group *SectorGroup
}

type Link struct {
	kind string // WALK, DROPOFF, TELEPORTER, DOOR, LIFT, BRIDGE or EMERGENCY
	name string // for debugging

	C1, C2 *Cluster
}

//----------------------------------------------------------------------

func (an *MapInfo) AnalyseMap() {
	// the order here matters!
	an.InitSectors()
	an.InitLines()
	an.InitThings()

	// detect some important map elements
	an.FindMovers()
	an.FindTeleporters()

	// WISH : jumps across (MAP02)
	// WISH : raising stairs

	// create Areas and Clusters from significant points
	an.CreateAreas()
	an.CreateClusters()
	an.LinkClusters()

	/// an.debugAsTextures()
}

func (an *MapInfo) InitThings() {
	an.things = make([]*AnThing, len(an.lev.Things))

	for idx, raw := range an.lev.Things {
		// def will commonly be nil for stuff like decorations.
		// sec can occasionally be nil for things in the void.
		def := Game.EntityByType(int(raw.Kind))
		sec := PointToSector(an.lev, int(raw.X), int(raw.Y))

		var asec *AnSector
		if sec != nil {
			asec = an.sectors[sec.Index]
		}

		athing := &AnThing{raw: raw, def: def, sector: asec}

		an.things[idx] = athing
	}
}

func (an *MapInfo) InitSectors() {
	an.sectors = make([]*AnSector, len(an.lev.Sectors))

	for idx, raw := range an.lev.Sectors {
		asec := &AnSector{raw: raw}

		asec.damaging = Game.SectorIsDamaging(int(asec.raw.Special))

		an.sectors[idx] = asec
	}
}

func (an *MapInfo) InitLines() {
	an.lines = make([]*AnLine, len(an.lev.Lines))

	for idx, raw := range an.lev.Lines {
		aline := &AnLine{raw: raw}

		aline.kind = Game.LineSpecialKind(int(raw.Special))

		if raw.Front != nil {
			aline.front = an.sectors[raw.Front.Sector.Index]
		}

		if raw.Back != nil {
			aline.back = an.sectors[raw.Back.Sector.Index]
		}

		an.lines[idx] = aline
	}
}

func (an *MapInfo) FindTeleporters() {
	for _, aline := range an.lines {
		if aline.front == nil || aline.back == nil {
			continue
		}

		var dest *AnSector

		if aline.kind == "TELEPORT" {
			// this may be nil, that is OK
			dest = an.FindTeleportDest(aline)
		}

		// line-to-line teleporters are a BOOM feature
		if aline.kind == "LINE_TELEPORT" {
			dest = an.FindLineTeleDest(aline)
		}

		if dest != nil {
			src := aline.front
			if src.tele_dests == nil {
				src.tele_dests = NewSectorGroup()
			}
			src.tele_dests.Add(dest)
		}
	}
}

func (an *MapInfo) FindTeleportDest(aline *AnLine) *AnSector {
	for _, athing := range an.things {
		if athing.raw.Kind == 14 &&
			athing.sector != nil &&
			athing.sector.raw.Tag == aline.raw.Tag {

			return athing.sector
		}
	}

	// not found
	return nil
}

func (an *MapInfo) FindLineTeleDest(aline *AnLine) *AnSector {
	for _, aline2 := range an.lines {
		if aline2.raw.Tag == aline.raw.Tag &&
			aline2.raw != aline.raw &&
			aline2.front != nil && aline2.back != nil {

			return aline2.front
		}
	}

	// not found
	return nil
}

func (an *MapInfo) FindMovers() {
	for _, aline := range an.lines {
		var mode string

		switch aline.kind {
		case "DOOR", "CEILING_UP":
			mode = "DOOR"
		case "LOCKED_DOOR":
			mode = "LOCKED_DOOR"
		case "FLOOR_DOWN", "FLOOR_UP":
			mode = "BRIDGE"
		case "LIFT":
			mode = "LIFT"
		case "BOOM_ELEVATOR", "PERPETUAL_FLOOR":
			mode = "MOVER"
		}

		if mode == "" {
			continue
		}

		act := Game.LineActionKind(int(aline.raw.Special))

		switch act {
		case "MANUAL":
			// manual doors always use the backside of the line
			if aline.front == nil || aline.back == nil {
				break
			}

			an.ProcessMover(aline.back, aline, mode)

		default:
			// zero tag is invalid
			if aline.raw.Tag <= 0 {
				break
			}

			for _, asec := range an.sectors {
				if asec.raw.Tag == aline.raw.Tag {
					an.ProcessMover(asec, aline, mode)
				}
			}
		}
	}
}

func (an *MapInfo) ProcessMover(asec *AnSector, activator *AnLine, mode string) {
	// Note: lowering and faising floors are handled here as "bridges".
	// They are similar to doors in how they provide access between two
	// same-height sectors which isn't there initially.

	neighbors := NewSectorGroup()

	// look for touching sectors
	for _, aline := range an.lines {
		if aline.front == nil || aline.back == nil || aline.front == aline.back {
			continue
		}
		if !(aline.front == asec || aline.back == asec) {
			continue
		}

		// check blocking flags
		if aline.raw.Flags&(wad.LF_Blocking|wad.ZLF_BlockPlayers|wad.ZLF_BlockAll) != 0 {
			continue
		}

		touch := aline.front
		if touch == asec {
			touch = aline.back
		}

		// player can fit in neighbor sector?
		if touch.raw.CeilH < touch.raw.FloorH + MIN_HEIGHT {
			continue
		}

		// check compatible floor heights for doors
		diff := int(touch.raw.FloorH) - int(asec.raw.FloorH)
		if (mode == "DOOR" || mode == "LOCKED_DOOR") && Abs(diff) > 24 {
			continue
		}

		// NOTE: doing a CanCrossLine() here (with updated ceiling height)
		// is probably too conservative and would miss some doors.

		neighbors.Add(touch)
	}

	// install results
	for _, src := range neighbors.sectors {
		for _, dest := range neighbors.sectors {
			if src == dest {
				continue
			}

			// require each side to *NOT* be walkable
			if an.AlreadyWalkable(src, dest) {
				continue
			}

			if mode == "MOVER" {
				if src.mover_dests == nil {
					src.mover_dests = NewSectorGroup()
				}
				src.mover_dests.Add(dest)

			} else if mode == "LIFT" {
				// require destination is above sourec
				if src.raw.FloorH >= dest.raw.FloorH {
					continue
				}
				// require height of lift itself is close to destination
				diff := int(dest.raw.FloorH) - int(asec.raw.FloorH)
				if Abs(diff) > 24 {
					continue
				}
				if src.lift_dests == nil {
					src.lift_dests = NewSectorGroup()
				}
				src.lift_dests.Add(dest)

			} else if mode == "BRIDGE" {
				// require each side of bridge have roughly same height
				diff := int(src.raw.FloorH) - int(dest.raw.FloorH)
				if Abs(diff) > 24 {
					continue
				}

				// check destination height is in the right direction
				if (activator.kind == "FLOOR_UP" && src.raw.FloorH <= asec.raw.FloorH) ||
					(activator.kind == "FLOOR_DOWN" && src.raw.FloorH >= asec.raw.FloorH) {
					continue
				}

				if src.bridge_dests == nil {
					src.bridge_dests = NewSectorGroup()
				}
				src.bridge_dests.Add(dest)

			} else if mode == "LOCKED_DOOR" {
				if src.locked_dests == nil {
					src.locked_dests = NewSectorGroup()
				}
				src.locked_dests.Add(dest)

			} else /* mode == "DOOR" */ {
				if src.door_dests == nil {
					src.door_dests = NewSectorGroup()
				}
				src.door_dests.Add(dest)
			}
		}
	}
}

func (an *MapInfo) AlreadyWalkable(S1, S2 *AnSector) bool {
	for _, aline := range an.lines {
		if aline.front == S1 && aline.back == S2 {
			// ok
		} else if aline.front == S2 && aline.back == S1 {
			// ok
		} else {
			continue
		}

		if CanCrossLine(aline.raw, false) {
			return true
		}
	}

	return false
}

//------------------------------------------------------------------------

func (an *MapInfo) CreateAreas() {
	for _, asec := range an.sectors {
		if asec.area != nil {
			continue
		}

		// too low for travel
		if asec.raw.CeilH - asec.raw.FloorH < MIN_HEIGHT {
			continue
		}

		area := new(Area)
		area.name = AllocId("AREA")

		area.group = NewSectorGroup()
		area.group.Add(asec)

		asec.area = area

		for an.GrowArea() {
		}
	}
}

func (an *MapInfo) GrowArea() (changes bool) {
	for _, aline := range an.lines {
		if aline.front == nil || aline.back == nil || aline.front == aline.back {
			continue
		}

		asec1 := aline.front
		asec2 := aline.back

		if asec1.raw.FloorH != asec2.raw.FloorH {
			continue
		}

		A1 := asec1.area
		A2 := asec2.area

		// require one to be nil, the other non-nil
		if (A1 == nil) == (A2 == nil) {
			continue
		}

		if A1 == nil {
			asec1, asec2 = asec2, asec1
			A1, A2 = A2, A1
		}

		if CanCrossLine(aline.raw, false) {
			A1.group.Add(asec2)
			asec2.area = A1

			changes = true
		}
	}

	return
}

func (an *MapInfo) CreateClusters() {
	an.clusters = make([]*Cluster, 0)

	// handle normal starts
	an.VisitStarts(false)

	// handle exits
	an.VisitExits()

	// handle voodoo closets
	an.VisitStarts(true)

	// handle other kinds
	an.VisitSecrets()
	an.VisitItems()
	an.VisitSwitches(false)

	// handle doors, lifts, teleporters etc
	an.VisitJoiners()

	// handle monster cages / ledges / closets
	// [ this must be done last ]
	an.VisitMonsters()

	// debugging stats
	if false {
		var avg_areas float64
		var max_areas int
		for _, cl := range an.clusters {
			avg_areas += float64(len(cl.areas))
			max_areas = Max(max_areas, len(cl.areas))
		}
		avg_areas /= float64(len(an.clusters))

		fmt.Printf("%s : %d clusters, avg areas %1.1f, max %d\n", an.name,
			len(an.clusters), avg_areas, max_areas)
	}
}

func (an *MapInfo) VisitStarts(voodoo bool) {
	// we need to find the *last* occurring player of each kind (1..4).
	// all other duplicate player starts will be voodoo dolls.
	var players [4]*AnThing

	for _, athing := range an.things {
		id := athing.raw.Kind
		if id >= 1 && id <= 4 &&
			(players[id-1] == nil || athing.raw.Index > players[id-1].raw.Index) {

			players[id-1] = athing
		}
	}

	for _, athing := range an.things {
		id := athing.raw.Kind
		if id >= 1 && id <= 4 && !voodoo && players[id-1] == athing {
			an.BeginCluster(athing.sector, "NORMAL", "START", true)
		}
	}

	for _, athing := range an.things {
		id := athing.raw.Kind
		if id >= 1 && id <= 4 && voodoo && players[id-1] != athing {
			an.BeginCluster(athing.sector, "VOODOO", "", true)
		}
	}
}

func (an *MapInfo) VisitExits() {
	for _, asec := range an.sectors {
		if asec.raw.Special == 11 {
			an.BeginCluster(asec, "NORMAL", "EXIT", true)
		}
	}

	an.VisitSwitches(true)
}

func (an *MapInfo) VisitSwitches(exit_only bool) {
	for _, aline := range an.lines {
		asec := aline.front
		if asec == nil || aline.kind == "" {
			continue
		}

		// require switch activation
		act := Game.LineActionKind(int(aline.raw.Special))
		if act != "SWITCH" && !exit_only {
			continue
		}

		// detect switches mounted inside a wall
		sw_raw := LineSwitchSector(an.lev, aline.raw)

		if sw_raw != nil {
			asec = an.sectors[sw_raw.Index]
		}

		if exit_only {
			if aline.kind == "EXIT" {
				an.BeginCluster(asec, "NORMAL", "EXIT", true)
			}
			continue
		}

		if aline.kind == "SECRET_EXIT" {
			an.BeginCluster(asec, "NORMAL", "SECRET", true)
			continue
		}

		// some lifts and doors are used to get out of a nukage pit
		// (where you normally wouldn't go), so ignore damaging
		// sectors here.
		if asec.damaging {
			continue
		}

		an.BeginCluster(asec, "NORMAL", "SWITCH", true)
	}
}

func (an *MapInfo) VisitSecrets() {
	for _, asec := range an.sectors {
		if asec.raw.Special == 9 {
			an.BeginCluster(asec, "NORMAL", "SECRET", true)
		}
	}
}

func (an *MapInfo) VisitItems() {
	for _, athing := range an.things {
		// item has to be available in single player
		if (athing.raw.Flags & wad.DTF_Not_SP) != 0 {
			continue
		}

		if athing.def == nil || athing.sector == nil {
			continue
		}

		// things in a damaging sector may just be there to help
		// a little bit (like the radiation suit), and don't
		// necessarily mean the player is meant to go there.
		// BUT... weapons and keys are an exception.
		if athing.sector.damaging {
			if athing.def.kind == ENT_Weapon {
				// ok
			} else if athing.def.kind == ENT_Pickup && athing.def.pickup.Kind == "KEY" {
				// ok
			} else {
				continue
			}
		}

		if athing.def.kind == ENT_Weapon || athing.def.kind == ENT_Pickup {
			an.BeginCluster(athing.sector, "NORMAL", "ITEM", true)
		}
	}
}

func (an *MapInfo) VisitJoiners() {
	// repeat until no more clusters can be made
	for {
		changes := false

		for _, S1 := range an.sectors {
			if S1.area == nil || S1.area.cluster == nil {
				continue
			}

			changes = changes || an.VisitOneJoiner(S1, S1.tele_dests, "TELEPORTER")
			changes = changes || an.VisitOneJoiner(S1, S1.door_dests, "DOOR")
			changes = changes || an.VisitOneJoiner(S1, S1.locked_dests, "LOCKED_DOOR")

			changes = changes || an.VisitOneJoiner(S1, S1.lift_dests, "LIFT")
			changes = changes || an.VisitOneJoiner(S1, S1.bridge_dests, "BRIDGE")
			changes = changes || an.VisitOneJoiner(S1, S1.mover_dests, "MOVER")
		}

		if !changes {
			break
		}
	}
}

func (an *MapInfo) VisitOneJoiner(S1 *AnSector, dests *SectorGroup, kind string) bool {
	if dests == nil {
		return false
	}

	changes := false

	for _, S2 := range dests.sectors {
		if S2.area == nil {
			continue
		}

		if S2.area.cluster == nil {
			changes = changes || an.BeginCluster(S2, "NORMAL", "JOINER", true)
		}

		if S2.area.cluster != nil {
			S1.area.cluster.AddOutLink(S2.area.cluster, kind)
		}
	}

	return changes
}

func (cl *Cluster) AddOutLink(C2 *Cluster, kind string) {
	// skip if already exists
	for _, link := range cl.out_links {
		if link.C1 == cl && link.C2 == C2 && link.kind == kind {
			return
		}
	}

	link := NewLink(kind, cl, C2)

	cl.out_links = append(cl.out_links, link)

	/*
		fmt.Printf("LINKED %s -> %s : %s\n", cl.name, C2.lanem kind)
	*/
}

func (an *MapInfo) VisitMonsters() {
	for _, athing := range an.things {
		// monster has to exist in single player
		if (athing.raw.Flags & wad.DTF_Not_SP) != 0 {
			continue
		}

		if athing.def == nil || athing.sector == nil {
			continue
		}

		if athing.def.kind == ENT_Monster {
			an.BeginCluster(athing.sector, "CLOSET", "", true)
		}
	}
}

func NewCluster(kind, sub_kind string) *Cluster {
	cl := new(Cluster)

	cl.kind = kind
	cl.sub_kind = sub_kind
	cl.name = AllocId("CLUSTER")

	cl.areas = make([]*Area, 0)
	cl.out_links = make([]*Link, 0)

	cl.mon_spots.Init()
	cl.item_spots.Init()
	cl.big_spots.Init()

	cl.bunches = make([]*Bunch, 0)
	cl.items = make([]*PickupDef, 0)
	cl.weapons = make([]*WeaponDef, 0)

	return cl
}

func (an *MapInfo) BeginCluster(asec *AnSector, kind, sub_kind string, grow_it bool) bool {
	// ignore unvisitable sectors
	if asec == nil || asec.area == nil {
		return false
	}

	// already in use?
	if asec.area.cluster != nil {
		return false
	}

	cl := NewCluster(kind, sub_kind)

	// leave link_set nil for monster and voodoo closets
	if kind == "NORMAL" {
		cl.link_set = cl
	}

	asec.area.cluster = cl

	cl.areas = append(cl.areas, asec.area)

	an.clusters = append(an.clusters, cl)

	if sub_kind == "START" && an.start_cluster == nil {
		an.start_cluster = cl
	}
	if sub_kind == "EXIT" && an.exit_cluster == nil {
		an.exit_cluster = cl
	}

	if grow_it {
		for an.GrowClusters() {
		}
	}

	return true
}

func (an *MapInfo) GrowClusters() (changes bool) {
	for _, aline := range an.lines {
		if aline.front == nil || aline.back == nil || aline.front == aline.back {
			continue
		}

		asec1 := aline.front
		asec2 := aline.back

		A1 := asec1.area
		A2 := asec2.area

		if A1 == nil || A2 == nil {
			continue
		}

		// require one to be nil, the other non-nil
		if (A1.cluster == nil) == (A2.cluster == nil) {
			continue
		}

		if A1.cluster == nil {
			asec1, asec2 = asec2, asec1
			A1, A2 = A2, A1
		}

		if CanCrossLine(aline.raw, false) {
			A2.cluster = A1.cluster
			A2.cluster.areas = append(A2.cluster.areas, A2)

			changes = true
		}
	}

	return
}

func (an *MapInfo) LinkClusters() {
	// no start == no point
	if an.start_cluster == nil {
		return
	}

	// at here, "DROPOFF" is the only valid kind of link not yet created,
	// so look for them now.
	an.LinkDropoffs()

	// give monster closets a cluster link for fight sim
	an.LinkMonsterClosets()

	// we need to handle cases where a cluster or group of clusters is
	// not yet reachable from the start cluster.
	// some strategies for finding emergency links are:
	//   - look at clusters neighboring a tagged sector
	//   - look for clusters separated by a linedef
	//   - pick two arbitrary clusters to link

	// find initial set of clusters reachable from the start
	an.LinkFromStart(an.start_cluster)

	// guard against infinite loop
	for loop := 0; !an.CheckAllLinked() && loop < 500; loop++ {
		if an.LinkLax_TaggedSectors() {
			continue
		}

		if an.LinkLax_CrossLine() {
			continue
		}

		// pick a single pair of clusters to link
		an.LinkLax_Emergency()
	}
}

func (an *MapInfo) LinkDropoffs() {
	for _, aline := range an.lines {
		S1 := aline.front
		S2 := aline.back

		if S1 == nil || S2 == nil || S1 == S2 {
			continue
		}
		if S1.area == nil || S2.area == nil {
			continue
		}

		cl1 := S1.area.cluster
		cl2 := S2.area.cluster

		if cl1 == nil || cl1.kind != "NORMAL" {
			continue
		}
		if cl2 == nil || cl2.kind != "NORMAL" {
			continue
		}
		if cl1 == cl2 {
			continue
		}

		// actually a dropoff?
		if S1.raw.FloorH >= S2.raw.FloorH+24 {
			// yes
		} else if S2.raw.FloorH >= S1.raw.FloorH+24 {
			// yes
			S1, S2 = S2, S1
			cl1, cl2 = cl2, cl1
		} else {
			// no
			continue
		}

		if CanCrossLine_IgnoreFloor(aline.raw) {
			cl1.AddOutLink(cl2, "DROPOFF")
		}
	}
}

func (an *MapInfo) LinkMonsterClosets() {
	for _, aline := range an.lines {
		S1 := aline.front
		S2 := aline.back

		if S1 == nil || S2 == nil || S1 == S2 {
			continue
		}
		if S1.area == nil || S2.area == nil {
			continue
		}

		cl1 := S1.area.cluster
		cl2 := S2.area.cluster

		if cl1 == nil || cl2 == nil || cl1 == cl2 {
			continue
		}

		if cl2.kind == "CLOSET" {
			S1, S2 = S2, S1
			cl1, cl2 = cl2, cl1
		}

		if !(cl1.kind == "CLOSET" && cl2.kind == "NORMAL") {
			continue
		}

		// already has a link?
		if len(cl1.out_links) > 0 {
			continue
		}

		// open space between the two sectors?
		f_min := Min(int(S1.raw.FloorH), int(S2.raw.FloorH))
		c_min := Min(int(S1.raw.CeilH), int(S2.raw.CeilH))

		if c_min > f_min {
			cl1.AddOutLink(cl2, "CLOSET_FACER")

			// update sub_kind of monster closet
			if (aline.raw.Flags & wad.LF_Blocking) != 0 {
				cl1.sub_kind = "CAGE"
			} else {
				cl1.sub_kind = "LEDGE"
			}
			/*
				fmt.Printf("MONSTER %s in sec %d facing %d\n", cl1.sub_kind, S1.raw.Index, S2.raw.Index)
			*/
		}
	}

	/* secondary method : teleporters */

	for _, S1 := range an.sectors {
		if S1.area == nil || S1.area.cluster == nil {
			continue
		}
		if S1.tele_dests == nil {
			continue
		}

		cl1 := S1.area.cluster

		if cl1.kind != "CLOSET" {
			continue
		}

		// already has a link?
		if len(cl1.out_links) > 0 {
			continue
		}

		for _, S2 := range S1.tele_dests.sectors {
			if S2.area == nil || S2.area.cluster == nil {
				continue
			}

			cl2 := S2.area.cluster
			if cl2.kind != "NORMAL" {
				continue
			}

			cl1.AddOutLink(cl2, "CLOSET_FACER")
			cl1.sub_kind = "TELEPORT"
			/*
				fmt.Printf("MONSTER %s in sec %d facing %d\n", cl1.sub_kind, S1.raw.Index, S2.raw.Index)
			*/
			break
		}
	}

	/* tertiary method : a opening in a wall */

	for _, asec := range an.sectors {
		if asec.raw.Tag > 0 {
			an.LinkClosetViaTagged(asec)
		}
	}

	/* emergencies */

	for _, cl := range an.clusters {
		if cl.kind == "CLOSET" && len(cl.out_links) == 0 {
			// WISH : choose cluster which is "closest"
			facer := an.start_cluster

			cl.AddOutLink(facer, "CLOSET_FACER")
		}
	}
}

func (an *MapInfo) LinkClosetViaTagged(asec *AnSector) {
	neighbors := NewSectorGroup()

	for _, aline := range an.lines {
		if aline.front == nil || aline.back == nil || aline.front == aline.back {
			continue
		}

		var touch *AnSector

		if aline.front == asec {
			touch = aline.back
		} else if aline.back == asec {
			touch = aline.front
		} else {
			continue
		}

		if touch.area == nil || touch.area.cluster == nil {
			continue
		}

		neighbors.Add(touch)
	}

	for _, S1 := range neighbors.sectors {
		cl1 := S1.area.cluster

		if cl1.kind != "CLOSET" || len(cl1.out_links) > 0 {
			continue
		}

		for _, S2 := range neighbors.sectors {
			cl2 := S2.area.cluster

			if cl2.kind != "NORMAL" {
				continue
			}

			cl1.AddOutLink(cl2, "CLOSET_FACER")
			cl1.sub_kind = "CLOSET"
			/*
				fmt.Printf("MONSTER %s in sec %d facing %d\n", cl1.sub_kind, S1.raw.Index, S2.raw.Index)
			*/
			break
		}
	}
}

func (an *MapInfo) LinkFromStart(cl *Cluster) {
	for _, link := range cl.out_links {
		nb := link.C2

		if nb.link_set != cl.link_set {
			// merge link *first* to prevent coming back
			an.MergeLink(link)

			// recursively visit neighbors
			an.LinkFromStart(nb)
		}
	}
}

func (an *MapInfo) LinkLax_TaggedSectors() bool {
	changes := false
	for _, asec := range an.sectors {
		if asec.raw.Tag > 0 {
			if an.LinkLax_TryTagged(asec) {
				changes = true
			}
		}
	}
	return changes
}

func (an *MapInfo) LinkLax_TryTagged(asec *AnSector) bool {
	neighbors := NewSectorGroup()

	for _, aline := range an.lines {
		if aline.front == nil || aline.back == nil || aline.front == aline.back {
			continue
		}

		var touch *AnSector

		if aline.front == asec {
			touch = aline.back
		} else if aline.back == asec {
			touch = aline.front
		} else {
			continue
		}

		if touch.area == nil || touch.area.cluster == nil {
			continue
		}

		neighbors.Add(touch)
	}

	changes := false

	for _, S1 := range neighbors.sectors {
		cl1 := S1.area.cluster
		if cl1.link_set != an.start_cluster.link_set {
			continue
		}

		for _, S2 := range neighbors.sectors {
			cl2 := S2.area.cluster

			if !(cl1.kind == "NORMAL" && cl2.kind == "NORMAL") {
				continue
			}
			// require one cluster in start group, other not
			if cl2.link_set == an.start_cluster.link_set {
				continue
			}

			cl1.AddOutLink(cl2, "LAX_SECTOR")

			// this will notice the new link and merge the groups
			an.LinkFromStart(cl1)

			changes = true
		}
	}

	return changes
}

func (an *MapInfo) LinkLax_CrossLine() bool {
	for _, aline := range an.lines {
		if aline.front == nil || aline.back == nil || aline.front == aline.back {
			continue
		}

		S1 := aline.front
		S2 := aline.back

		if S1.area == nil || S2.area == nil {
			continue
		}

		cl1 := S1.area.cluster
		cl2 := S2.area.cluster

		if cl1 == nil || cl2 == nil || cl1 == cl2 {
			continue
		}
		if cl1.kind != "NORMAL" || cl2.kind != "NORMAL" {
			continue
		}

		if cl2.link_set == an.start_cluster.link_set {
			S1, S2 = S2, S1
			cl1, cl2 = cl2, cl1
		}

		if cl1.link_set != an.start_cluster.link_set {
			continue
		}
		if cl2.link_set == an.start_cluster.link_set {
			continue
		}

		cl1.AddOutLink(cl2, "LAX_LINE")

		// this will notice the new link and merge the groups
		an.LinkFromStart(cl1)

		// only do one at a time
		return true
	}

	return false
}

func (an *MapInfo) LinkLax_Emergency() bool {
	for _, cl := range an.clusters {
		if cl.kind != "NORMAL" {
			continue
		}

		if cl.link_set == an.start_cluster.link_set {
			continue
		}

		// WISH : choose cluster which is "closest"
		var from *Cluster
		for _, cl2 := range an.clusters {
			if cl2.kind == "NORMAL" && cl2.link_set == an.start_cluster.link_set {
				from = cl2
			}
		}
		from.AddOutLink(cl, "EMERGENCY")

		// this will notice the new link and merge the groups
		an.LinkFromStart(from)

		// only do one at a time
		return true
	}

	return false
}

func (an *MapInfo) CheckAllLinked() bool {
	for _, cl := range an.clusters {
		if cl.kind == "NORMAL" && cl.link_set != an.start_cluster.link_set {
			return false
		}
	}
	return true
}

func (an *MapInfo) MergeLink(link *Link) {
	ls1 := link.C1.link_set
	ls2 := link.C2.link_set

	an.MergeSets(ls1, ls2)
}

func (an *MapInfo) MergeSets(ls1, ls2 *Cluster) {
	if ls1 == ls2 {
		return
	}

	for _, cl := range an.clusters {
		if cl.link_set == ls2 {
			cl.link_set = ls1
		}
	}
}

func (an *MapInfo) StartGroupSize() int {
	count := 0
	for _, cl := range an.clusters {
		if cl.link_set == an.start_cluster.link_set {
			count++
		}
	}
	return count
}

func NewLink(kind string, C1, C2 *Cluster) *Link {
	link := new(Link)

	link.kind = kind
	link.name = AllocId("LINK")

	link.C1 = C1
	link.C2 = C2

	return link
}

// modify the floor textures to show the traverse groups.
func (an *MapInfo) debugAsTextures() {
	for _, asec := range an.sectors {
		tex := "CEIL4_1" // a dark texture

		if asec.area != nil && asec.area.cluster != nil {
			// tex = asec.area.cluster.kind
			tex = asec.area.cluster.debugTex()
		}

		asec.raw.FloorTex = tex
	}
}

func (cl *Cluster) debugTex() string {
	switch cl.kind {
	case "VOODOO":
		return "LAVA1"

	case "CLOSET":
		return "FLAT4"

	default: // NORMAL
		switch cl.sub_kind {
			case "START":
				return "MFLR8_3" // whiteish
			case "EXIT":
				return "DEM1_3" // greenish
			case "SECRET":
				return "FLOOR1_6" // reddish
			case "SWITCH":
				return "FLAT14" // blueish
			case "JOINER":
				return "NUKAGE1" // dark green
			default:
				return "FLAT5" // brownish
		}
	}
}
