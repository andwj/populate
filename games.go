// Copyright 2019 Andrew Apted.
// This code is under the GNU General Public License, version 3
// or (at your option) any later version.

/*

Loader for game definitions.

*/

package main

import "os"
import "fmt"
import "path/filepath"

import "gitlab.com/andwj/ini"

// GameDef defines misc properties of a particular game.
type GameDef struct {
	Format string // can be DOOM or HEXEN

	Jump_height float64 // how high the player can jump, 0 if not at all

	Initial_weapons []string

	Sector_specials map[int]string
	Line_specials   map[int]string
	Activations     map[int]string
}

// MonsterDef defines the properties of a single enemy.
type MonsterDef struct {
	name string

	Type int     // editor number used to place monster on the map

	R      float64 // radius of monster
	H      float64 // height of monster
	Health float64 // hit points of monster
	Damage float64 // total damage inflicted on player (on average)

	Appear     float64 // how far along (over episode) it should appear (1..9)
	Prob       float64 // chance of adding into level (relative to other mons)
	Toughness  float64 // measure of how tough the monster is (1 to 100)
	Skip_prob  float64 // chance of skipping this monster in a map

	Melee     bool // TRUE if monster only has a close-range attack
	Flying    bool // TRUE if monster can fly
	Invisible bool // TRUE if monster is partially invisible

	Species        string  // name of species (default is monster itself)
	Disloyal       bool    // TRIE if can hurt a member of same species
	Infight_damage float64 // damage inflicted on one (or more) other monsters

	Drop_weapon string  // a weapon dropped by the monster
	Drop_ammo   float64 // the ammo for the weapon dropped
}

// WeaponDef defines the properties of a single weapon.
type WeaponDef struct {
	name string

	Type   int     // editor number used to place weapon on the map

	Appear float64 // how far along (over episode) it should appear (1..9)
	Prob   float64 // probabiliiy of adding into level (after first appearance)
	Usage  float64 // usage preference (relative to other weapons)

	Fire_rate float64   // shots per second
	Damage    float64   // damage can inflict per shot (assuming good accuracy)
	Splash    []float64 // splash damage to other monsters (2nd, 3rd, etc)

	Ammo_type     string  // name of ammo, e.g. "BULLET"
	Ammo_per_shot float64 // amount of ammo used when firing the weapon
	Give_ammo     float64 // ammo given when weapon is picked up
}

// PickupDef defines the properties of a pickup item.
type PickupDef struct {
	name string

	Type int // editor number used to place item on the map

	Kind        string  // can be: HEALTH / ARMOR / AMMO / POWERUP / KEY
	Ammo_type   string  // name of ammo
	Give_ammo   float64 // amount of ammo given
	Give_health float64 // ammount of health given

	Prob    float64 // chance of adding as a general pickup [absent = never]
	Rank    float64 // the general niceness of the item (1..4)
	Cluster float64 // how many to place together
}

type GameInfo struct {
	name string

	base GameDef

	monsters []*MonsterDef
	weapons  []*WeaponDef
	pickups  []*PickupDef

	entities map[int]*EntityDef
}

type EntityKind int

const (
	ENT_UNKNOWN EntityKind = iota

	ENT_Monster
	ENT_Weapon
	ENT_Pickup
)

type EntityDef struct {
	Type int
	name string
	kind EntityKind

	// zero or one of these is a valid pointer
	monster *MonsterDef
	weapon  *WeaponDef
	pickup  *PickupDef
}

//----------------------------------------------------------------------

func LoadGame(game string) (*GameInfo, error) {
	fmt.Printf("loading game: %s\n", game)

	g := &GameInfo{name: game}

	if err := g.LoadGameDef(); err != nil {
		return nil, err
	}
	if err := g.LoadMonsters(); err != nil {
		return nil, err
	}
	if err := g.LoadWeapons(); err != nil {
		return nil, err
	}
	if err := g.LoadPickups(); err != nil {
		return nil, err
	}

	g.BuildEntityTable()

	return g, Ok
}

func (g *GameInfo) LoadGameDef() error {
	filename := filepath.Join(Options.install, g.name, "Game.ini")

	f, err := os.Open(filename)
	if err != nil {
		fmt.Fprintf(os.Stderr, "invalid game: %s (could not find 'Game.ini' file)\n", g.name)
		return err
	}
	defer f.Close()

	r := ini.NewReader(f)

	r.RegisterVar("GAME", &g.base)

	err = ReadINI_ShowError("Game.ini", r)
	if err != nil {
		return err
	}

	return Ok
}

func (g *GameInfo) LoadMonsters() error {
	g.monsters = make([]*MonsterDef, 0, 50)

	filename := filepath.Join(Options.install, g.name, "Monsters.ini")

	f, err := os.Open(filename)
	if err != nil {
		fmt.Fprintf(os.Stderr, "invalid game: %s (no 'Monsters.ini' file)\n", g.name)
		return err
	}
	defer f.Close()

	r := ini.NewReader(f)

	var dummy MonsterDef
	r.RegisterType("*", &dummy,
		func(rec_ptr interface{}, name, suffix string) {
			mon := rec_ptr.(*MonsterDef)
			mon.name = name
			g.monsters = append(g.monsters, mon)
		})

	return ReadINI_ShowError("Monsters.ini", r)
}

func (g *GameInfo) LoadWeapons() error {
	g.weapons = make([]*WeaponDef, 0, 50)

	filename := filepath.Join(Options.install, g.name, "Weapons.ini")

	f, err := os.Open(filename)
	if err != nil {
		fmt.Fprintf(os.Stderr, "invalid game: %s (no 'Weapons.ini' file)\n", g.name)
		return err
	}
	defer f.Close()

	r := ini.NewReader(f)

	var dummy WeaponDef
	r.RegisterType("*", &dummy,
		func(rec_ptr interface{}, name, suffix string) {
			wp := rec_ptr.(*WeaponDef)
			wp.name = name
			g.weapons = append(g.weapons, wp)
		})

	return ReadINI_ShowError("Weapons.ini", r)
}

func (g *GameInfo) LoadPickups() error {
	g.pickups = make([]*PickupDef, 0, 50)

	filename := filepath.Join(Options.install, g.name, "Pickups.ini")

	f, err := os.Open(filename)
	if err != nil {
		fmt.Fprintf(os.Stderr, "invalid game: %s (no 'Pickups.ini' file)\n", g.name)
		return err
	}
	defer f.Close()

	r := ini.NewReader(f)

	var dummy PickupDef
	r.RegisterType("*", &dummy,
		func(rec_ptr interface{}, name, suffix string) {
			pu := rec_ptr.(*PickupDef)
			pu.name = name
			g.pickups = append(g.pickups, pu)
		})

	return ReadINI_ShowError("Pickups.ini", r)
}

// ReadINI_ShowError calls ReadAll on the INI reader and shows more
// information on an error.
func ReadINI_ShowError(filename string, r *ini.Reader) error {
	err := r.ReadAll()

	if err == nil {
		return Ok
	}

	fmt.Fprintf(os.Stderr, "Error reading %s\n", filename)

	if r.ErrorRecordName() != "" {
		fmt.Fprintf(os.Stderr, "... in [%s] record\n", r.ErrorRecordName())
	}

	if r.ErrorFieldName() != "" {
		fmt.Fprintf(os.Stderr, "... in field '%s'\n", r.ErrorFieldName())
	}

	fmt.Fprintf(os.Stderr, "... on line %d\n", r.ErrorLineNum())

	switch err {
	case ini.ErrParse:
		fmt.Fprintf(os.Stderr, "Parsing error: %s\n", r.ErrorParseInfo())
	case ini.ErrUnknownRecord, ini.ErrUnknownField:
		fmt.Fprintf(os.Stderr, "Parsing error: %s\n", err.Error())
	default:
		fmt.Fprintf(os.Stderr, "I/O error: %s\n", err.Error())
	}

	return err
}

//----------------------------------------------------------------------

func (g *GameInfo) EntityByType(typ int) *EntityDef {
	return g.entities[typ]
}

func (g *GameInfo) MonsterByType(typ int) *MonsterDef {
	if e := g.entities[typ]; e != nil {
		return e.monster
	}
	return nil
}

func (g *GameInfo) WeaponByType(typ int) *WeaponDef {
	if e := g.entities[typ]; e != nil {
		return e.weapon
	}
	return nil
}

func (g *GameInfo) WeaponByName(name string) *WeaponDef {
	for _, wp := range g.weapons {
		if wp.name == name {
			return wp
		}
	}
	return nil
}

func (g *GameInfo) PickupByType(typ int) *PickupDef {
	if e := g.entities[typ]; e != nil {
		return e.pickup
	}
	return nil
}

func (g *GameInfo) BuildEntityTable() {
	g.entities = make(map[int]*EntityDef)

	for _, mon := range g.monsters {
		if mon.Type != 0 {
			e := &EntityDef{Type: mon.Type, name: mon.name, kind: ENT_Monster,
				monster: mon}
			g.entities[mon.Type] = e
		}
	}
	for _, wp := range g.weapons {
		if wp.Type != 0 {
			e := &EntityDef{Type: wp.Type, name: wp.name, kind: ENT_Weapon,
				weapon: wp}
			g.entities[wp.Type] = e
		}
	}
	for _, pu := range g.pickups {
		if pu.Type != 0 {
			e := &EntityDef{Type: pu.Type, name: pu.name, kind: ENT_Pickup,
				pickup: pu}
			g.entities[pu.Type] = e
		}
	}
}

func (g *GameInfo) LineSpecialKind(typ int) string {
	// returns "" if unknown

	// WISH: handle BOOM generalized types

	return g.base.Line_specials[typ]
}

func (g *GameInfo) LineActionKind(typ int) string {
	// returns "" if unknown

	// WISH: handle BOOM generalized types

	return g.base.Activations[typ]
}

func (g *GameInfo) SectorIsDamaging(typ int) bool {
	// WISH: handle BOOM generalized types

	if g.base.Sector_specials[typ] == "DAMAGE" {
		return true
	}
	return false
}
