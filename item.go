// Copyright 2019 Andrew Apted.
// This code is under the GNU General Public License, version 3
// or (at your option) any later version.

/*

Item selection and placement

*/

package main

//import "fmt"
import "math"
import "math/rand"
import "sort"

import "gitlab.com/andwj/wad"

type ItemSupply struct {
	used_weapons map[string]int
}

type Weapon struct {
	def *WeaponDef

	// normally 1.0 -- can be lower or higher to create a "weapon palette"
	// for a section of that map.  should not be zero though.
	usage_factor float64
}

type Pickup struct {
	def   *PickupDef
	count int
}

type PickupList struct {
	pickups []*Pickup
}

// PlayerModel models the health, weapons, and ammo which the
// player in the game would have at some particular point.
type PlayerModel struct {
	weapons []*Weapon
	stats   Stats
}

func (m *MapInfo) Weaponize() {
	m.used_weapons = make(map[string]int)

	for _, athing := range m.things {
		// must be a weapon
		if athing.def == nil || athing.def.kind != ENT_Weapon {
			continue
		}

		var cl *Cluster
		if athing.sector != nil && athing.sector.area != nil {
			cl = athing.sector.area.cluster
		} else {
			// WISH : find a nearby cluster
			cl = m.start_cluster
		}

		// weapon has to be available in single player
		if (athing.raw.Flags & wad.DTF_Not_SP) != 0 {
			athing.discard = true
			continue
		}

		// weapon must be available in UV skill
		if (athing.raw.Flags & wad.TF_Hard) == 0 {
			athing.discard = true
			continue
		}

		// all skills should have the same weapons
		athing.raw.Flags |= wad.TF_Easy | wad.TF_Medium

		/* Ok */

		wp_def := m.SelectWeapon()

		if wp_def == nil {
			athing.discard = true
			continue
		}

		m.used_weapons[wp_def.name] = m.used_weapons[wp_def.name] + 1

		// replace the existing thing
		athing.raw.Kind = uint16(wp_def.Type)
		athing.def = Game.EntityByType(int(athing.raw.Kind))

		if cl != nil {
			cl.weapons = append(cl.weapons, wp_def)
		}
	}
}

func (m *MapInfo) SelectWeapon() *WeaponDef {
	prob_tab := make([]float64, len(Game.weapons))

	total := 0

	for idx, def := range Game.weapons {
		prob := def.Prob

		// too early in the mapset for this weapon?
		if def.Appear > m.item_along {
			prob = 0
		}

		if m.used_weapons[def.name] >= 2 {
			prob /= 100
		} else if m.used_weapons[def.name] >= 1 {
			prob /= 5
		}

		prob_tab[idx] = prob

		if prob > 0 {
			total++
		}
	}

	if total == 0 {
		return nil
	}

	idx := IndexByProbs(prob_tab)

	return Game.weapons[idx]
}

//----------------------------------------------------------------------

func (m *MapInfo) Itemize() {
	m.FilterPickups()

	if m.start_cluster == nil {
		return
	}

	model := NewPlayerModel()

	m.BattleInCluster(m.start_cluster, model)
}

func (m *MapInfo) FilterPickups() {
	for _, athing := range m.things {
		if athing.def == nil || athing.def.kind != ENT_Pickup {
			continue
		}

		var cl *Cluster
		if athing.sector != nil && athing.sector.area != nil {
			cl = athing.sector.area.cluster
		} else {
			// only happens when pickup is in the void / closed sector
			continue
		}

		def := athing.def.pickup

		// item has to be available in single player
		if (athing.raw.Flags & wad.DTF_Not_SP) != 0 {
			athing.discard = true
			continue
		}

		// item must be available in medium skill
		if (athing.raw.Flags & wad.TF_Medium) == 0 {
			athing.discard = true
			continue
		}
		athing.raw.Flags |= wad.TF_Easy | wad.TF_Hard

		// leave powerups alone [ for now... ]
		if def.Kind == "POWERUP" {
			continue
		}

		// leave armor alone, but remember how much health it gave
		// NOTE: megasphere is marked as HEALTH
		if def.Kind == "ARMOR" {
			if cl != nil {
				cl.items = append(cl.items, def)
			}
			continue
		}

		// leave big health pickups alone, but remember for the sim
		if def.Kind == "HEALTH" && def.Rank >= 3 {
			if cl != nil {
				cl.items = append(cl.items, def)
			}
			continue
		}

		// remove ammo and health, but leave keys alone
		if def.Kind == "AMMO" || def.Kind == "HEALTH" {
			athing.discard = true

			// this code is currently useless
			/*
			if def.Rank >= 3 && cl != nil {
				spot := new(Spot)
				spot.x = int(athing.raw.X)
				spot.y = int(athing.raw.Y)
				spot.r = 20
				spot.h = 64

				cl.big_spots.Add(spot, athing.sector.area)
			}
			*/
		}
	}
}

func (m *MapInfo) BattleInCluster(cl *Cluster, model *PlayerModel) {
	// give items in cluster
	for _, wp_def := range cl.weapons {
		model.AddWeapon(wp_def)
	}

	for _, pu_def := range cl.items {
		model.GiveItem(pu_def, 1)
	}

	stats := NewStats()

	m.SimulateBattle(cl, model, &stats)

	// simulate battles in monster closets linked to here
	for _, closet := range m.clusters {
		if closet.kind == "CLOSET" &&
			len(closet.out_links) > 0 &&
			closet.out_links[0].C2 == cl {

			m.SimulateBattle(cl, model, &stats)
		}
	}

	m.PickupsForCluster(cl, model, &stats)

	cl.battled = true

	// recursively visit neighboring clusters
	for _, link := range cl.out_links {
		if !link.C2.battled	{
			m.BattleInCluster(link.C2, model)
		}
	}
}

func (m *MapInfo) SimulateBattle(cl *Cluster, model *PlayerModel, stats *Stats) {
	for _, wp := range model.weapons {
		wp.usage_factor = 1.0
	}

	// give monster drops
	// [ do it before in order to get dropped weapons ]
	for _, bunch := range cl.bunches {
		if bunch.count > 0 {
			model.GiveDropStuff(bunch.def, bunch.count)
		}
	}

	FightSimulator(cl.bunches, model.weapons, stats)
	/*
		fmt.Printf("battle in %s (%s), %d mons --> %s\n", cl.name, cl.kind,
			len(mon_list), stats.String())
	*/
}

func (m *MapInfo) PickupsForCluster(cl *Cluster, model *PlayerModel, stats *Stats) {
	var list PickupList
	list.pickups = make([]*Pickup, 0)

	// reduce player model by needed stats.
	// this means stats in PlayerModel often become negative.
	for _, value := range stats.values {
		model.stats.AddTo(value.k, -value.v)
	}

	// decide items
	for _, value := range model.stats.values {
		// target for health is higher than zero (but not 100)
		qty := 0 - value.v
		if value.k == "HEALTH" {
			qty = 30 - value.v
		}

		m.PickupsForStat(value.k, qty, &list)
	}

	// sort items by rank, highest first
	sort.SliceStable(list.pickups, func(i, k int) bool {
		pu1 := list.pickups[i]
		pu2 := list.pickups[k]

		return pu1.def.Rank > pu2.def.Rank
	})

	// normal item spots were shuffled earlier
	cl.big_spots.Shuffle()

	for _, pu := range list.pickups {
		m.PlacePickup(pu, cl)

		model.GiveItem(pu.def, pu.count)
	}

	/*
		println("player:", model.stats.String())
	*/
}

func (m *MapInfo) PickupsForStat(stat string, qty float64, list *PickupList) {
	for qty > 0 {
		pu := m.SelectPickup(stat, qty)
		if pu == nil {
			break
		}

		list.pickups = append(list.pickups, pu)

		// guard against infinite looping here
		qty -= math.Max(0.1, pu.Quantity())
	}
}

func (m *MapInfo) SelectPickup(stat string, qty float64) *Pickup {
	prob_tab := make([]float64, len(Game.pickups))

	total := 0

	for idx, def := range Game.pickups {
		prob := def.Prob

		if stat == "HEALTH" && def.Kind == stat {
			// inhibit big-ticket items unless needed health is great
			if def.Rank >= 3 && qty < def.Give_health * 2 {
				prob = 0
			}
		} else if stat != "HEALTH" && def.Kind == "AMMO" && def.Ammo_type == stat {
			// ok
		} else {
			prob = 0
		}

		prob_tab[idx] = prob

		if prob > 0 {
			total++
		}
	}

	if total == 0 {
		return nil
	}

	idx := IndexByProbs(prob_tab)

	pu := new(Pickup)
	pu.def = Game.pickups[idx]
	pu.count = 1

	if pu.def.Cluster > 0 {
		low := int(pu.def.Cluster)
		high := low

		if high > 3 {
			low--
			high++
		}

		pu.count = Between(low, high)

		for pu.count > 1 && pu.Quantity() > qty {
			pu.count--
		}
	}

	return pu
}

func (pu *Pickup) Quantity() float64 {
	if pu.def.Kind == "HEALTH" {
		return pu.def.Give_health * float64(pu.count)
	} else {
		return pu.def.Give_ammo * float64(pu.count)
	}
}

func (m *MapInfo) PlacePickup(pu *Pickup, cl *Cluster) {
	flags := wad.TF_Easy | wad.TF_Medium | wad.TF_Hard

	// prefer a big spot for big items
	if pu.def.Rank >= 2 && pu.count == 1 {
		sp := m.GetBigSpot(cl)
		if sp != nil {
			athing := m.AddThing(sp.x, sp.y, pu.def.Type, flags)
			_ = athing
			return
		}
	}

	var last *Spot

	for i := 0; i < pu.count; i++ {
		sp := m.GetSpotForPickup(cl, last)
		if sp == nil {
			break
		}

		athing := m.AddThing(sp.x, sp.y, pu.def.Type, flags)
		_ = athing

		last = sp
	}
}

func (m *MapInfo) GetSpotForPickup(cl *Cluster, last *Spot) *Spot {
	var best *Spot
	best_dist := 9e9

	for _, sp := range cl.item_spots.spots {
		if sp.used {
			continue
		}

		if last == nil {
			best = sp
			break
		}

		// WISH : if spot is near a big item, skip it

		// for item clusters, we want spots close together.
		// this formula allows diagonals.
		dx := Abs(sp.x - last.x)
		dy := Abs(sp.y - last.y)
		dist := float64(Max(dx, dy))

		// tie-breaker
		dist += rand.Float64() / 16

		if dist < best_dist {
			best = sp
			best_dist = dist
		}
	}

	if best != nil {
		best.used = true
		return best
	}

	cl.item_spots.Init()

	// cannabilize monster spots
	for _, sp := range cl.mon_spots.spots {
		if sp.used {
			continue
		}

		// WISH : if spot is near a big item, skip it

		sp = cl.mon_spots.SplitSpot(sp, 32)
		sp.used = true

		return sp
	}

	cl.mon_spots.Init()

	return nil
}

func (m *MapInfo) GetBigSpot(cl *Cluster) *Spot {
	for _, sp := range cl.big_spots.spots {
		if sp.used {
			continue
		}
		sp.used = true
		return sp
	}

	for _, sp := range cl.mon_spots.spots {
		if sp.used {
			continue
		}

		// WISH : if spot is near a big item, skip it

		sp = cl.mon_spots.SplitSpot(sp, 64)
		sp.used = true

		return sp
	}

	// none left, and that's OK
	return nil
}

//----------------------------------------------------------------------

func NewPlayerModel() *PlayerModel {
	model := new(PlayerModel)
	model.weapons = make([]*Weapon, 0)
	model.stats = NewStats()

	// give initial weapons
	for _, wp_name := range Game.base.Initial_weapons {
		def := Game.WeaponByName(wp_name)
		if def != nil {
			model.AddWeapon(def)
		}
	}

	// give initial health
	model.stats.Set("HEALTH", 100)

	return model
}

func (model *PlayerModel) AddWeapon(def *WeaponDef) {
	// already have it?
	for _, exist := range model.weapons {
		if exist.def == def {
			return
		}
	}

	wp := new(Weapon)
	wp.def = def

	model.weapons = append(model.weapons, wp)
}

func (model *PlayerModel) GiveItem(def *PickupDef, count int) {
	var kind string
	var qty  float64

	switch def.Kind {
	case "HEALTH", "ARMOR":
		kind = "HEALTH"
		qty = def.Give_health

	case "AMMO":
		kind = def.Ammo_type
		qty = def.Give_ammo

	default:
		// nothing else is modelled
		return
	}

	model.stats.AddTo(kind, qty * float64(count))
}

func (model *PlayerModel) GiveDropStuff(mon_def *MonsterDef, count int) {
	if mon_def.Drop_weapon == "" {
		return
	}

	wp_def := Game.WeaponByName(mon_def.Drop_weapon)
	if wp_def == nil {
		// oops, mistake in game config
		return
	}

	model.AddWeapon(wp_def)

	if wp_def.Ammo_type == "" || wp_def.Ammo_type == "NONE" {
		return
	}

	model.stats.AddTo(wp_def.Ammo_type, mon_def.Drop_ammo * float64(count))
}
