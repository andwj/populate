// Copyright 2019 Andrew Apted.
// This code is under the GNU General Public License, version 3
// or (at your option) any later version.

/*

Spot logic -- determine spots where monsters and items can be
placed, by "drawing" sectors in a grid and then analysing the
open spaces.

*/

package main

import "os"
import "io"
import "fmt"
import "math"
import "math/rand"
import "sort"

import "gitlab.com/andwj/wad"

// Spot represents a area of the level where monsters and/or
// pickup items can be placed.  Spots may be quite large, and
// hence may be split into smaller ones when used.
type Spot struct {
	// location: center on XY plane, bottom in Z axis
	x, y, z int

	// radius (half the extents on XY plane), and height
	r, h int

	// the area containing this spot
	area *Area

	// spot has already been used
	used bool
}

func (sp *Spot) String() string {
	return fmt.Sprintf("[%d %d %d r:%d h:%d]", sp.x, sp.y, sp.z, sp.r, sp.h)
}

type SpotList struct {
	spots []*Spot
}

// CellType is value in a single square in the spot grid.
type CellType byte

// values for CellType type.
const (
	// CLEAR signifies a place which can be traversed, and
	// where monsters and items can be put.
	SPOT_CLEAR = CellType(iota)

	// LOW signifies a place where the ceiling is quite low,
	// high enough for the player but too low for tall monsters
	// to fit.
	SPOT_LOW

	// EDGE signifies a place where the sector group ends,
	// but which is not blocked.  Main examples are the top of
	// a cliff, or next to a stair that is a bit higher than
	// the sector group (but not high enough to be blocking).
	SPOT_EDGE

	// BLOCKED signifies an impassible barrier of some kind,
	// such as a solid wall, or the bottom of a cliff, or when
	// the ceiling is too low for the player to fit.
	SPOT_BLOCKED

	// the ITEM bitflag marks item spots (to prevent re-use)
	SPOTFLAG_ITEM CellType = 0x10

	// the MON bitflag marks monster spots and dud cells
	SPOTFLAG_MON CellType = 0x20
)

// GRID_SIZE is the maximum size of the grid
const GRID_SIZE = 1024

// PAD is how many extra cells to use on each side (top, bottom,
// left and right).
const PAD = 2

// Grid contains the result of rendering the group of sectors.
type Grid struct {
	cells [GRID_SIZE][GRID_SIZE]CellType

	// size used for current render (# of cells).
	// [ the cells outside of this box will not be modified ]
	width  int
	height int

	// the bounding box of the sector group
	bbox BBox

	// size of each cell (in DOOM units)
	cell_size int
}

//----------------------------------------------------------------------

// have a single global Grid, to play nice with the garbage collector
var grid Grid

// these constant are good for DOOM maps
const MIN_ITEM_SIZE = 24
const MIN_MON_SIZE = 40
const MAX_MON_SIZE = 256

const MIN_HEIGHT = 64    // no monster can fit below this
const CLEAR_HEIGHT = 128 // sectors below this are SPOT_LOW, else SPOT_CLEAR
const CLIFF_HEIGHT = 64  // when back sector is higher than front by at least this

// SpotRender takes a group of sectors and "renders" them into the
// global 'grid' variable.  If the result would be empty, then this
// returns false (indicating nothing was rendered).
func (m *MapInfo) SpotRender(area *Area) bool {
	grid.width = 0
	grid.height = 0
	grid.cell_size = 0

	group := area.group
	if group == nil {
		return false
	}

	// check if sector group would be completely blocking
	// [ this is an optimization ]
	valid_secs := 0
	for _, asec := range group.sectors {
		if (asec.raw.CeilH - asec.raw.FloorH) >= MIN_HEIGHT {
			valid_secs++
		}
	}
	if valid_secs == 0 {
		return false
	}

	// compute bounding box of the group
	grid.bbox.Clear()

	for _, aline := range m.lines {
		ld := aline.raw
		if group.HasLine(aline) {
			grid.bbox.AddPoint(int(ld.Start.X), int(ld.Start.Y))
			grid.bbox.AddPoint(int(ld.End.X), int(ld.End.Y))
		}
	}

	if !grid.bbox.Valid {
		return false
	}

	// very small sectors won't be able to hold anything
	if (grid.bbox.X2-grid.bbox.X1) < 48 ||
		(grid.bbox.Y2-grid.bbox.Y1) < 48 {
		return false
	}

	// determine width and height in cells, trying a series of
	// increasingly larger cell sizes.  in the VERY unlikely event
	// that the biggest size is still too small, we will truncate
	// the final render.

	for pass := 0; pass < 4; pass++ {
		switch pass {
		case 0:
			grid.cell_size = 8
		case 1:
			grid.cell_size = 12
		case 2:
			grid.cell_size = 20
		case 3:
			grid.cell_size = 32
		}

		gx2, gy2 := grid.Coord(grid.bbox.X2, grid.bbox.Y2)

		grid.width = int(gx2) + PAD
		grid.height = int(gy2) + PAD

		if grid.width < GRID_SIZE && grid.height < GRID_SIZE {
			break
		}
	}

	// truncate if too large
	grid.width = Min(grid.width, GRID_SIZE)
	grid.height = Min(grid.height, GRID_SIZE)

	// initialize the grid to be SPOT_EDGE.
	// [ we don't use SPOT_BLOCKED because of item spots ]
	grid.Clear(SPOT_EDGE)

	// for all the sectors in the group, fill them with SPOT_CLEAR
	// or SPOT_LOW (depending on their heights).  note that this
	// produces areas which are somewhat too large.

	// Note: need to do SPOT_LOW *after* SPOT_CLEAR
	grid.RenderGroup(m, group, SPOT_CLEAR)
	grid.RenderGroup(m, group, SPOT_LOW)

	// use line-drawing to handle the boundaries of the sector group,
	// applying SPOT_LOW, SPOT_EDGE or SPOT_BLOCKED as appropriate.

	for _, aline := range m.lines {
		grid.RenderBoundary(aline, group)
	}

	// iterate over entities and mark blocking ones with SPOT_EDGE.
	// ceiling-hanging entities ideally would be marked as SPOT_LOW.
	// we never mark entities as SPOT_BLOCKED (due to item placement).

	for _, athing := range m.things {
		if athing.discard {
			continue
		}
		// monsters kept from the existing level need to be marked too
		// [ since large ones can overlap multiple sectors ]
		if athing.def == nil || athing.def.kind == ENT_Monster {
			r := 32
			h := 128 // not used

			if athing.def != nil && athing.def.monster != nil {
				r = Max(r, int(athing.def.monster.R))
			}

			grid.RenderEntity(athing.raw, r, h)
		}
	}

	return true
}

func (g *Grid) Coord(x, y int) (gx, gy float64) {
	gx = float64(x-g.bbox.X1)/float64(g.cell_size) + PAD
	gy = float64(y-g.bbox.Y1)/float64(g.cell_size) + PAD
	return
}

func (g *Grid) MapCoord(gx, gy float64) (x, y int) {
	x = g.bbox.X1 + int((gx-PAD)*float64(g.cell_size))
	y = g.bbox.Y1 + int((gy-PAD)*float64(g.cell_size))
	return
}

func (g *Grid) Clear(c CellType) {
	for x := 0; x < g.width; x++ {
		for y := 0; y < g.height; y++ {
			g.cells[x][y] = c
		}
	}
}

func (g *Grid) Dump(w io.Writer) {
	fmt.Fprintln(w, "Spot Grid:")

	charForCell := func(c CellType) string {
		switch c & 7 {
		case SPOT_CLEAR:
			return "."
		case SPOT_LOW:
			return "^"
		case SPOT_EDGE:
			return "/"
		case SPOT_BLOCKED:
			return "#"
		default:
			panic("charForCell hit default case")
		}
	}

	for y := g.height - 1; y >= 0; y-- {
		line := ""
		for x := 0; x < g.width; x++ {
			line += charForCell(g.cells[x][y])
		}
		fmt.Fprintln(w, line)
	}

	fmt.Fprintln(w)
}

// DumpPPM creates a PPM image file
func (g *Grid) DumpPPM(filename string) error {
	f, err := os.Create(filename)
	if err != nil {
		return err
	}

	defer f.Close()

	fmt.Fprintf(f, "P6 %d %d 255 ", g.width, g.height)

	colorForCell := func(c CellType) (r, g, b byte) {
		switch c & 7 {
		case SPOT_CLEAR:
			return 255, 255, 255 // white
		case SPOT_LOW:
			return 0, 255, 0 // green
		case SPOT_EDGE:
			return 0, 0, 0 // black
		case SPOT_BLOCKED:
			return 255, 0, 0 // red
		default:
			panic("colorForCell hit default case")
		}
	}

	for y := g.height - 1; y >= 0; y-- {
		for x := 0; x < g.width; x++ {
			var buf [3]byte
			buf[0], buf[1], buf[2] = colorForCell(g.cells[x][y])
			f.Write(buf[:])
		}
	}

	return Ok
}

func (g *Grid) RenderEntity(th *wad.Thing, r, h int) {
	x1, y1 := int(th.X) - r, int(th.Y) - r
	x2, y2 := int(th.X) + r, int(th.Y) + r

	g.DrawBox(x1, y1, x2, y2, SPOT_EDGE)
}

func (g *Grid) RenderBoundary(aline *AnLine, group *SectorGroup) {
	// ignore any lines not touching the group
	if !group.HasLine(aline) {
		return
	}

	ld := aline.raw

	if aline.front == nil || aline.back == nil {
		g.DrawLine(ld, SPOT_BLOCKED)
		return
	}

	const BlockFlags = wad.LF_Blocking | wad.LF_BlockMonsters |
		wad.ZLF_BlockPlayers | wad.ZLF_BlockAll

	if ld.Flags&BlockFlags != 0 {
		g.DrawLine(ld, SPOT_EDGE)
		return
	}

	// from here on, require the line to actually be "boundary"
	if !group.IsBoundary(aline) {
		return
	}

	// check for bottom of a cliff
	f := ld.Front.Sector
	b := ld.Back.Sector

	if !group.Has(aline.front) {
		f, b = b, f
	}

	if b.FloorH >= f.FloorH+CLIFF_HEIGHT {
		// yep!
		g.DrawLine(ld, SPOT_BLOCKED)
		return
	}

	g.DrawLine(ld, SPOT_EDGE)
}

func (g *Grid) DrawLine(ld *wad.LineDef, c CellType) {
	x1 := int(ld.Start.X)
	y1 := int(ld.Start.Y)
	x2 := int(ld.End.X)
	y2 := int(ld.End.Y)

	// cull if completely outside of bounding box
	if int(Min(x1, x2)) > g.bbox.X2+1 ||
		int(Min(y1, y2)) > g.bbox.Y2+1 ||
		int(Max(x1, x2)) < g.bbox.X1-1 ||
		int(Max(y1, y2)) < g.bbox.Y1-1 {
		return
	}

	gx1, gy1 := g.Coord(x1, y1)
	gx2, gy2 := g.Coord(x2, y2)
	/*
		if gy1 > gy2 {
			gx1, gx2 = gx2, gx1
			gy1, gy2 = gy2, gy1
		}
	*/
	g.rawLineSegment(gx1, gy1, gx2, gy2, c)
}

func (g *Grid) rawLineSegment(gx1, gy1, gx2, gy2 float64, c CellType) {
	// NOTE: this code is quite lame, but seems to work OK

	if math.Abs(gx1-gx2) >= 2.0 || math.Abs(gy1-gy2) >= 2.0 {
		mx := (gx1 + gx2) * 0.5
		my := (gy1 + gy2) * 0.5

		g.rawLineSegment(gx1, gy1, mx, my, c)
		g.rawLineSegment(mx, my, gx2, gy2, c)
	} else {
		if gx1 > gx2 {
			gx1, gx2 = gx2, gx1
		}
		if gy1 > gy2 {
			gy1, gy2 = gy2, gy1
		}

		g.rawBox(gx1, gy1, gx2, gy2, c)
	}
}

func (g *Grid) DrawBox(x1, y1, x2, y2 int, c CellType) {
	gx1, gy1 := g.Coord(x1, y1)
	gx2, gy2 := g.Coord(x2, y2)

	if gx1 > gx2 {
		gx1, gx2 = gx2, gx1
	}
	if gy1 > gy2 {
		gy1, gy2 = gy2, gy1
	}

	g.rawBox(gx1, gy1, gx2, gy2, c)
}

func (g *Grid) rawBox(gx1, gy1, gx2, gy2 float64, c CellType) {
	px1, py1 := int(gx1), int(gy1)
	px2, py2 := int(gx2), int(gy2)

	// clip to usable coords.
	// [ this may make px1 > px2 or py1 > py2, but that is OK ]
	px1 = Max(px1, 0)
	py1 = Max(py1, 0)

	px2 = Min(px2, g.width-1)
	py2 = Min(py2, g.height-1)

	for px := px1; px <= px2; px++ {
		for py := py1; py <= py2; py++ {
			g.UpdateCell(px, py, c)
		}
	}
}

func (g *Grid) FillTrapezoid(gy1, gy2 float64, left *renderEdge,
	right *renderEdge, c CellType) {

	// called "Fill" because we always set the cell type
	// [ we don't call g.UpdateCell because we need CLEAR
	//   values to replace the initial EDGE values ]

	// quantize and clip
	py1 := Max(int(gy1), 0)
	py2 := Min(int(gy2), g.height-1)

	for py := py1; py <= py2; py++ {
		lx, _ := left.RowMinMax(py)
		_, rx := right.RowMinMax(py)

		// quantize and clip
		px1 := Max(int(lx), 0)
		px2 := Min(int(rx), g.width-1)

		for px := px1; px <= px2; px++ {
			g.cells[px][py] = c
		}
	}
}

const X_EPSILON = 0.01
const Y_EPSILON = 0.001

type renderEdge struct {
	// coordinates mapped to the grid, not clipped
	gx1, gy1 float64
	gx2, gy2 float64

	// what side this edge faces: -1 for left, +1 for right
	side int

	// an "active" edge is one that covers the current Y range
	// (when finding trapezoids)
	active bool

	// X coordinate for the current Y range
	active_x float64
}

func (re *renderEdge) Covers(gy1, gy2 float64) bool {
	return true &&
		(re.gy1 < gy1+Y_EPSILON) &&
		(re.gy2 > gy2-Y_EPSILON)
}

func (re *renderEdge) CalcX(y float64) float64 {
	return re.gx1 + (re.gx2-re.gx1)*(y-re.gy1)/(re.gy2-re.gy1)
}

func (re *renderEdge) RowMinMax(py int) (float64, float64) {
	y1 := math.Max(re.gy1, float64(py))
	y2 := math.Min(re.gy2, float64(py+1))

	x1 := re.CalcX(y1)
	x2 := re.CalcX(y2)

	return x1, x2
}

func (re *renderEdge) String() string {
	side := "R"
	if re.side < 0 {
		side = "L"
	}
	return fmt.Sprintf("%s(%+7.2f %+7.2f .. %+7.2f %+7.2f)", side,
		re.gx1, re.gy1, re.gx2, re.gy2)
}

func (g *Grid) RenderGroup(m *MapInfo, group *SectorGroup, c CellType) {
	//
	// ALGORITHM
	//
	// What this does is divide the sector group into trapezoids with
	// purely horizontal tops and bottoms, using a fairly well known
	// method of iterating over edges sorted vertically.
	//
	// Then each trapezoid is rendered to the grid, setting all cells
	// which the trapezoid "touches" (extends into).  The coordinates of
	// the trapezoids are fine, NOT quantized on the cell grid.
	//
	// I considered an alternate approach of working like a polygon
	// renderer, "scanning" horizontally at a single Y coord in each
	// row of cells.  However that method is not guaranteed to set all
	// cells which a sector group touches, hence it was ruled out.
	//

	// split the group based on pass
	group2 := NewSectorGroup()

	for _,asec := range group.sectors {
		sec := asec.raw

		is_blk := (sec.CeilH - sec.FloorH) < MIN_HEIGHT
		is_low := (sec.CeilH - sec.FloorH) < CLEAR_HEIGHT

		// ignore blocking sectors
		// [ as we don't need SPOT_BLOCK done for whole sectors ]
		if is_blk {
			continue
		}

		if is_low == (c == SPOT_LOW) {
			group2.Add(asec)
		}
	}

	// part 1: visit linedefs and create edges
	edges := make([]*renderEdge, 0, 100)

	for _, aline := range m.lines {
		if group2.IsBoundary(aline) {
			var f *AnSector

			if aline.front != nil {
				f = aline.front
			}
			//	if ld.Back != nil {
			//		b = ld.Back.Sector
			//	}

			side := 1

			if f == nil || !group2.Has(f) {
				side = -side
			}

			ld := aline.raw

			gx1, gy1 := g.Coord(int(ld.Start.X), int(ld.Start.Y))
			gx2, gy2 := g.Coord(int(ld.End.X), int(ld.End.Y))

			// ignore purely horizontal edges
			if math.Abs(gy1-gy2) < Y_EPSILON {
				continue
			}

			if gy1 > gy2 {
				gx1, gx2 = gx2, gx1
				gy1, gy2 = gy2, gy1
				side = -side
			}

			edge := renderEdge{gx1: gx1, gy1: gy1, gx2: gx2, gy2: gy2, side: side}

			edges = append(edges, &edge)
		}
	}

	// this can only happen with malformed level geometry
	if len(edges) < 2 {
		return
	}

	// sort edges by their bottom Y coordinate
	sort.Slice(edges,
		func(i, k int) bool {
			return edges[i].gy1 < edges[k].gy1
		})

	numEdges := len(edges)

	// debugging...
	if false {
		fmt.Printf("Edge list:\n")
		for _, edge := range edges {
			fmt.Printf("    %s\n", edge)
		}
	}

	// part 2: traverse edge list and create trapezoids
	//
	// each trapezoid has a purely horizontal top and bottom, and
	// sides which may be sloped or purely vertical.

	pos := 0
	gy1 := edges[pos].gy1

	for pos < numEdges-1 {
		// find the next highest Y coordinate to use.
		// it is OK if none is found in the initial search.
		// we also find the subset of edges we need to visit.
		gy2 := float64(1 << 30)

		var end int

		for end = pos + 1; end < numEdges; end++ {
			if edges[end].gy1 > gy1+Y_EPSILON {
				gy2 = edges[end].gy1

				// we can stop now because the edges are sorted
				break
			}
		}

		// update gy2 for the top Y of edges
		for k := pos; k < end; k++ {
			if edges[k].gy2 > gy1+Y_EPSILON {
				gy2 = math.Min(gy2, edges[k].gy2)
			}
		}

		// Note: if gy2 got updated, then we could move the current 'end'
		// position back to lower the number of edges we need to process.
		// But it may not be worth the extra effort.

		g.RenderTrapezoids(edges[pos:end], gy1, gy2, c)

		// move onto next trapezoid, and update 'pos' to skip any edges
		// lying completely below the new bottom Y.
		gy1 = gy2

		for pos < numEdges && edges[pos].gy2 < gy1+Y_EPSILON {
			pos++
		}
	}
}

func (g *Grid) RenderTrapezoids(edges []*renderEdge,
	gy1, gy2 float64, c CellType) {

	if false {
		fmt.Printf("Trapezoid Y range: %+7.2f .. %+7.2f\n", gy1, gy2)
		// return
	}

	midY := (gy1 + gy2) * 0.5

	// mark which edges cover the given range
	for _, edge := range edges {
		edge.active = edge.Covers(gy1, gy2)

		if edge.active {
			edge.active_x = edge.CalcX(midY)
		}
	}

	// sort the active trapezoids from left to right
	sortFunc := func(i, k int) bool {
		a := edges[i]
		b := edges[k]

		// this moves all inactive edges to the end
		if !(a.active && b.active) {
			return edges[i].active && !edges[k].active
		}

		return a.active_x < b.active_x
	}

	sort.Slice(edges, sortFunc)

	// shrink slice to ignore all the inactive edges
	for len(edges) > 0 && !edges[len(edges)-1].active {
		edges = edges[0 : len(edges)-1]
	}

	// debugging
	if false {
		fmt.Printf("%d Active edges:\n", len(edges))

		for _, edge := range edges {
			fmt.Printf("    %s\n", edge)
		}
	}

	// unless the map is broken (or a bug in the previous code),
	// our edge list should consist of a number of R/L pairs
	// (an edge facing right followed by an edge facing left).

	for i := 0; i < len(edges)-1; i++ {
		left := edges[i]
		right := edges[i+1]

		if left.side > 0 && right.side < 0 {
			g.FillTrapezoid(gy1, gy2, left, right, c)
		}
	}
}

func (g *Grid) IsValid(x, y int) bool {
	return true &&
		(x >= 0) && (x < g.width) &&
		(y >= 0) && (y < g.height)
}

func (g *Grid) UpdateCell(x, y int, c CellType) {
	if g.cells[x][y] < c {
		g.cells[x][y] = c
	}
}

//----------------------------------------------------------------------

func (g *Grid) FindItemSpots(list *SpotList, area *Area) {
	//
	// ALGORITHM
	//
	// This can be quite simple, due to the small size of item spots
	// and because they must be adjacent to a wall.
	//
	// We simply iterate over each usable cell, check if it is near a
	// wall, and if it is then do a comprehensive test around that cell.
	//

	for y := PAD; y < g.height-PAD; y++ {
		for x := PAD; x < g.width-PAD; x++ {
			c := g.cells[x][y]
			// slight optimization, we need SPOTFLAG_ITEM to be clear
			if c&(7|SPOTFLAG_ITEM) <= SPOT_LOW && g.NearWall(x, y) {
				if spot := g.TryItemSpot(x, y); spot != nil {
					list.Add(spot, area)
				}
			}
		}
	}
}

func (g *Grid) NearWall(x, y int) bool {
	var dir Direction

	for dir = 0; dir < 4; dir++ {
		nx, ny := Nudge(x, y, dir)
		if g.IsValid(nx, ny) && (g.cells[nx][ny]&7 == SPOT_BLOCKED) {
			return true
		}
	}

	return false
}

func (g *Grid) TryItemSpot(x, y int) *Spot {
	// determine the cell size of an item spot
	min_cells := int(math.Ceil(MIN_ITEM_SIZE / float64(g.cell_size)))

	// try all possibilities which contain the given point
	for sx := x - min_cells + 1; sx <= x; sx++ {
		for sy := y - min_cells + 1; sy <= y; sy++ {
			if g.TestItemBox(sx, sy, sx+min_cells-1, sy+min_cells-1) {
				// OK, create a spot
				// Note: TestItemBox has marked the area as used

				mx, my := g.MapCoord(
					float64(sx)+float64(min_cells)*0.5,
					float64(sy)+float64(min_cells)*0.5)

				// we don't really need the Z coordinate, since we don't
				// support 3D floors of DOOM source ports, or any truly 3D
				// games like Quake.
				mz := 0

				r := min_cells * g.cell_size / 2
				h := MIN_HEIGHT

				return &Spot{x: mx, y: my, z: mz, r: r, h: h}
			}
		}
	}

	// nothing possible
	return nil
}

func (g *Grid) TestItemBox(x1, y1, x2, y2 int) bool {
	// outside of the grid?
	if x1 < PAD || x2 >= g.width-PAD ||
		y1 < PAD || y2 >= g.height-PAD {
		return false
	}

	for x := x1; x <= x2; x++ {
		for y := y1; y <= y2; y++ {
			c := g.cells[x][y]
			if c&7 > SPOT_LOW || c&SPOTFLAG_ITEM != 0 {
				return false
			}
		}
	}

	// spot is OK, so mark the cells as used
	g.MarkWithFlag(x1, y1, x2, y2, SPOTFLAG_ITEM)

	return true
}

func (g *Grid) FindMonsterSpots(list *SpotList, area *Area) {
	g.FindMonsterSpotsForHeight(list, area, false)
	g.FindMonsterSpotsForHeight(list, area, true)
}

func (g *Grid) FindMonsterSpotsForHeight(list *SpotList, area *Area, is_low bool) {
	//
	// ALGORITHM
	//
	// We perform this in two main stages:
	//
	// The first stage looks for the largest open point in the grid,
	// and tests that point to grow the spot to the largest possible.
	// We only try this a limited number of times, as it can be quite
	// expensive.
	//
	// The second stage is where we simply iterate over ALL the cells
	// and try to grow the largest spot at each one.  We mark cells as
	// used as we go, even ones too small to be a valid spot.
	//

	min_cells := int(math.Ceil(MIN_MON_SIZE / float64(g.cell_size)))
	max_cells := int(math.Ceil(MAX_MON_SIZE / float64(g.cell_size)))

	want := SPOT_CLEAR
	if is_low {
		want = SPOT_LOW
	}

	/* --- stage 1 --- */

	try_num := 2 + g.width/10

	for t := 0; t < try_num; t++ {
		found, x, y := g.LargestGap(want)

		if !found {
			break
		}

		spot := g.LargestMonsterAt(want, x, y, true, min_cells, max_cells)
		if spot == nil {
			// failed, mark the cell so we won't try it again
			g.cells[x][y] |= SPOTFLAG_MON
			continue
		}

		// Note: LargestMonsterAt marked the box as used

		list.Add(spot, area)

		/* debugging
			fmt.Printf("Stage 1 mon spot: %s\n", spot)
		*/
	}

	/* --- stage 2 --- */

	for x := PAD; x < g.width-PAD; x++ {
		for y := PAD; y < g.height-PAD; y++ {
			// slight optimization, SPOTFLAG_MON must be unset
			if g.cells[x][y]&(7|SPOTFLAG_MON) == want {
				spot := g.LargestMonsterAt(want, x, y, false, min_cells, max_cells)

				if spot == nil {
					g.cells[x][y] |= SPOTFLAG_MON
					continue
				}

				list.Add(spot, area)

				/* debugging
					fmt.Printf("Stage 2 mon spot: %s\n", spot)
				*/
			}
		}
	}
}

func (g *Grid) LargestMonsterAt(want CellType, x1, y1 int,
	all_dirs bool, min_cells, max_cells int) *Spot {

	x2, y2 := x1, y1

	for {
		// too large?
		if (x2 - x1 + 1) >= max_cells {
			break
		}

		if g.CanExpandMonster(want, x1, y1, x2, y2, +1, +1) {
			x2++
			y2++
			continue
		}

		if !all_dirs {
			break
		}

		if g.CanExpandMonster(want, x1, y1, x2, y2, -1, +1) {
			x1--
			y2++
			continue
		} else if g.CanExpandMonster(want, x1, y1, x2, y2, +1, -1) {
			x2++
			y1--
			continue
		} else if g.CanExpandMonster(want, x1, y1, x2, y2, -1, -1) {
			x1--
			y1--
			continue
		} else {
			// nothing was possible
			break
		}
	}

	// large enough?
	if (x2 - x1 + 1) < min_cells {
		return nil
	}

	/* OK */

	g.MarkWithFlag(x1, y1, x2, y2, SPOTFLAG_MON)

	mx, my := g.MapCoord(
		float64(x1)+float64(x2-x1)*0.5,
		float64(y1)+float64(y2-y1)*0.5)

	// we don't really need the Z coordinate, since we don't
	// support 3D floors of DOOM source ports, or any truly 3D
	// games like Quake.
	mz := 0

	r := (x2 - x1 + 1) * g.cell_size / 2

	h := MIN_HEIGHT
	if want == SPOT_CLEAR {
		h = CLEAR_HEIGHT
	}

	return &Spot{x: mx, y: my, z: mz, r: r, h: h}
}

func (g *Grid) CanExpandMonster(want CellType, x1, y1, x2, y2 int,
	dx, dy int) bool {

	var x, y int

	if dx > 0 {
		x = x2 + 1
	} else {
		x = x1 - 1
	}

	if dy > 0 {
		y = y2 + 1
	} else {
		y = y1 - 1
	}

	if !g.IsValid(x, y) {
		return false
	}

	if g.cells[x][y]&(7|SPOTFLAG_MON) != want {
		return false
	}

	for tx := x1; tx <= x2; tx++ {
		if g.cells[tx][y]&(7|SPOTFLAG_MON) != want {
			return false
		}
	}

	for ty := y1; ty <= y2; ty++ {
		if g.cells[x][ty]&(7|SPOTFLAG_MON) != want {
			return false
		}
	}

	return true
}

func (g *Grid) LargestGap(want CellType) (found bool, res_x int, res_y int) {
	// minimum is 2 squares
	best := 1

	for y := PAD; y < g.height-PAD; y++ {
		for x := PAD; x < g.width-PAD; {
			// slight optimization, SPOTFLAG_MON must be unset
			if g.cells[x][y]&(7|SPOTFLAG_MON) != want {
				x++
				continue
			}

			sx := x
			ex := x

			for ; ex+1 < g.width-PAD; ex++ {
				if g.cells[ex+1][y]&(7|SPOTFLAG_MON) != want {
					break
				}
			}

			num := ex - sx + 1

			if num > best {
				best = num
				found = true
				res_x = (sx + ex) / 2
				res_y = y
			}

			x = ex + 1
		}
	}

	return
}

func (g *Grid) MarkWithFlag(x1, y1, x2, y2 int, flag CellType) {
	for x := x1; x <= x2; x++ {
		for y := y1; y <= y2; y++ {
			g.cells[x][y] |= flag
		}
	}
}

//----------------------------------------------------------------------

type SectorGroup struct {
	sectors []*AnSector

	// NOTE: do not iterate over this!
	present_secs map[*AnSector]bool
}

func NewSectorGroup() *SectorGroup {
	grp := new(SectorGroup)
	grp.sectors = make([]*AnSector, 0)
	grp.present_secs = make(map[*AnSector]bool)
	return grp
}

func (grp *SectorGroup) Has(asec *AnSector) bool {
	return grp.present_secs[asec]
}

func (grp *SectorGroup) Add(asec *AnSector) {
	if !grp.present_secs[asec] {
		grp.present_secs[asec] = true
		grp.sectors = append(grp.sectors, asec)
	}
}

func (grp *SectorGroup) Union(other *SectorGroup) {
	for _, sec := range other.sectors {
		grp.Add(sec)
	}
}

func (grp *SectorGroup) HasLine(aline *AnLine) bool {
	return (aline.front != nil && grp.Has(aline.front)) ||
		(aline.back != nil && grp.Has(aline.back))
}

func (grp *SectorGroup) IsBoundary(aline *AnLine) bool {
	if aline.front == nil || aline.back == nil {
		return grp.HasLine(aline)
	}

	has_front := grp.Has(aline.front)
	has_back := grp.Has(aline.back)

	return has_front != has_back
}

//----------------------------------------------------------------------

func (list *SpotList) Init() {
	list.spots = make([]*Spot, 0)
}

func (list *SpotList) Add(sp *Spot, area *Area) {
	sp.area = area
	list.spots = append(list.spots, sp)
}

// SplitSpot checks if the spot is much larger than the monster
// which will be placed in it, which happens often with large
// contiguous areas.  If so, the spot is split into a bunch of
// smaller spots (ones still big enough for the given monster
// radius) which are added into this spot list.
//
// When no splitting occurs, the original spot is returned,
// otherwise one of the new spots is chosen at random
// (and the original spot should not be used).
func (list *SpotList) SplitSpot(sp *Spot, r int) *Spot {
	// prefer power-of-two sizes, not too small
	if r <= 32 {
		r = 32
	} else if r <= 64 {
		r = 64
	} else {
		r = int(math.Ceil(float64(r) / 128) * 128)
	}

	// number of new spots on each axis
	N := int(math.Floor(float64(sp.r) / float64(r)))

	if N < 2 {
		return sp
	}

	rx := Rand(N)
	ry := Rand(N)
	result := sp

	x1 := float64(sp.x - sp.r + r)
	x2 := float64(sp.x + sp.r - r)

	y1 := float64(sp.y - sp.r + r)
	y2 := float64(sp.y + sp.r - r)

	for cx := 0; cx < N; cx++ {
		for cy := 0; cy < N; cy++ {
			new_sp := sp

			if cx > 0 || cy > 0 {
				new_sp = new(Spot)
				new_sp.z = sp.z
				new_sp.h = sp.h

				list.Add(new_sp, sp.area)
			}

			fx := x1 + (x2-x1) * float64(cx) / float64(N-1)
			fy := y1 + (y2-y1) * float64(cy) / float64(N-1)

			new_sp.x = int(fx)
			new_sp.y = int(fy)
			new_sp.r = r

			if cx == rx && cy == ry {
				result = new_sp
			}
		}
	}

	return result
}

func (list *SpotList) Shuffle() {
	rand.Shuffle(len(list.spots), func(i, k int) {
		A := list.spots[i]
		B := list.spots[k]

		list.spots[i] = B
		list.spots[k] = A
	})
}
