// Copyright 2019 Andrew Apted.
// This code is under the GNU General Public License, version 3
// or (at your option) any later version.

/*

Populator replaces the monsters in DOOM levels with ones
chosen at random, and adds a fresh set of weapons, health and
ammo items.

*/
package main

import "os"
import "fmt"
import "strings"
import "time"
import "math/rand"
import "path/filepath"
import "runtime"

import "gitlab.com/andwj/argv"

var Ok error

const VERSION = "0.71"

// command-line options
var Options struct {
	install  string
	game     string

	in_file  string
	out_file string
	cfg_file string

	seed     int

	keep_weapons bool

	help    bool
	version bool
}

// important globals
var Game *GameInfo

func parseArgs() {
	Options.install = "."
	Options.game = "doom2"

	argv.Generic("i", "input", &Options.in_file, "file", "input wad")
	argv.Generic("o", "output", &Options.out_file, "file", "output wad")
	argv.Gap()

	argv.Generic("", "install", &Options.install, "dir", "installation directory")
	argv.Generic("g", "game", &Options.game, "game", "target game (default is \"doom2\")")
	argv.Integer("s", "seed", &Options.seed, "num", "seed for random numbers")
	argv.Gap()

//	argv.Enabler("", "keep-weapons", &Options.keep_weapons, "keep the original weapons")

	argv.Enabler("h", "help", &Options.help, "display this help text")
	argv.Enabler("", "version", &Options.version, "display program version")

	err := argv.Parse()
	if err != nil {
		fmt.Fprintf(os.Stderr, "%s\n", err.Error())
		os.Exit(1)
	}

	if Options.help || len(os.Args) <= 1 {
		fmt.Println("USAGE: populator [-i] input.wad [-o output.wad] [OPTIONS...]")
		fmt.Println("OPTIONS:")
		argv.Display(os.Stdout)
		os.Exit(0)
	}

	if Options.version {
		// version already in the banner
		fmt.Println("(c) 2019 Andrew Apted, GPL v3 license")
		fmt.Printf("%s OS, %s architecture\n", runtime.GOOS, runtime.GOARCH)
		os.Exit(0)
	}

	unparsed := argv.Unparsed()

	if Options.in_file == "" {
		if len(unparsed) == 0 || unparsed[0] == "" {
			fmt.Fprintf(os.Stderr, "missing input filename!\n")
			os.Exit(1)
		}
		Options.in_file = unparsed[0]
		unparsed = unparsed[1:]
	}

	Options.in_file = filepath.Clean(Options.in_file)

	if len(unparsed) > 0 {
		fmt.Fprintf(os.Stderr, "too many filenames!\n")
		os.Exit(1)
	}

	if Options.out_file == "" {
		// we want to add "_pop" to the input filename, but keep
		// the existing file extension
		ext := filepath.Ext(Options.in_file)
		tmp := strings.TrimSuffix(Options.in_file, ext)

		Options.out_file = tmp + "_pop" + ext
	}

	if Options.out_file == Options.in_file {
		fmt.Fprintf(os.Stderr, "output filename same as input!\n")
		os.Exit(1)
	}

	if Options.cfg_file == "" {
		ext := filepath.Ext(Options.out_file)
		tmp := strings.TrimSuffix(Options.out_file, ext)

		Options.cfg_file = tmp + ".cfg"
	}

	if Options.seed == 0 {
		Options.seed = int(time.Now().Unix() & 0x7FFFFFFF)
	}
}

func main() {
	fmt.Printf("THE POPULATOR (version %s)\n", VERSION)

	// parse the command-line (this may exit the program)
	parseArgs()

	rand.Seed(int64(Options.seed))
	fmt.Printf("random seed: %d\n", Options.seed)

	var err error

	Game, err = LoadGame(Options.game)
	if err != nil {
		// LoadGame will display the error message
		os.Exit(1)
	}

	// IDEA: load/create a config file

	if !ProcessWadFile(Options.in_file, Options.out_file) {
		os.Exit(1)
	}

	fmt.Printf("done.\n")
}
