// Copyright 2019 Andrew Apted.
// This code is under the GNU General Public License, version 3
// or (at your option) any later version.

/*

Handling of DOOM/Hexen levels in a wad.

*/

package main

import "os"
import "fmt"
import "math"
import "math/rand"
import "sort"
import "strings"
import "runtime"

import "gitlab.com/andwj/wad"

type MapInfo struct {
	idx  int  // index into in.Directory
	name string

	lump *wad.Lump
	lev  *wad.Level

	Planning   // plan.go
	Analysis   // analyse.go
	Hostility  // enemy.go
	ItemSupply // item.go
}

func NewMapInfo(idx int, lump *wad.Lump) *MapInfo {
	lev := &MapInfo{lump: lump, name: lump.Name, idx: idx}
	return lev
}

func (m *MapInfo) IsBoss() bool {
	switch m.name {
	case "MAP07", "MAP30", "E1M8", "E2M8", "E3M8", "E4M8", "E5M8":
		return true
	}
	return false
}

func (m *MapInfo) IsSecret() bool {
	switch m.name {
	case "MAP31", "MAP32", "E1M9", "E2M9", "E3M9", "E4M9", "E5M9":
		return true
	}
	return false
}

//----------------------------------------------------------------------

// ProcessWadFile handles a whole WAD file, visiting each level and
// populating it, and saving the new level (plus other lumps) into
// the output file.
//
// If an error occurs, a message is printed and false is returned.
// On success, true is returned.
func ProcessWadFile(in_file, out_file string) bool {
	fmt.Printf("reading file:  %s\n", in_file)

	in, err := wad.Open(in_file)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to open input file: %s\n", in_file)
		fmt.Fprintf(os.Stderr, "--> %s\n", err.Error())
		return false
	}
	defer in.Close()

	fmt.Printf("creating file: %s\n", out_file)

	out, err := wad.Create(out_file)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to create output file: %s\n", out_file)
		fmt.Fprintf(os.Stderr, "--> %s\n", err.Error())
		return false
	}
	defer out.Close()

	out.Iwad = in.Iwad

	maps := CollectWadMaps(in)

	if len(maps) == 0 {
		fmt.Fprintf(os.Stderr, "No levels found in wad.\n")
		return false
	}

	// add info lump
	CreateInfoLump(out)

	Planner(maps)

	for idx, m := range maps {
		rand.Seed(int64(Options.seed + idx))

		if err := ProcessMap(in, out, m); err != nil {
			fmt.Fprintf(os.Stderr, "%s\n", err.Error())
			return false
		}
	}

	fmt.Printf("populated %d map(s)\n", len(maps))

	fmt.Printf("writing file....\n")

	// copy all non-level lumps from input to output wad
	for _, lump := range in.Directory {
		if !lump.LevelPart && lump.LevelMarker == wad.NOT_A_LEVEL {
			if err := CopyRawLump(in, out, lump); err != nil {
				fmt.Fprintf(os.Stderr, "%s\n", err.Error())
				return false
			}
		}
	}

	out.Finish()

	// Ok
	return true
}

func CollectWadMaps(in *wad.Wad) []*MapInfo {
	maps := make([]*MapInfo, 0, 50)

	for idx, lump := range in.Directory {
		if lump.LevelMarker != wad.NOT_A_LEVEL {
			m := NewMapInfo(idx, lump)
			maps = append(maps, m)
		}
	}

	// sort into alphabetical order
	sort.Slice(maps, func(i, k int) bool {
		return maps[i].name < maps[k].name
	})

	return maps
}

func CopyRawLump(in *wad.Wad, out *wad.Wad, lump *wad.Lump) error {
	// omit any existing POPULDAT lump
	if lump.Name == "POPULDAT" {
		return Ok
	}

	data, err := in.ReadLump(lump)
	if err != nil {
		return err
	}

	if err = out.WriteLump(lump.Name, data); err != nil {
		return err
	}

	return Ok
}

func ProcessMap(in *wad.Wad, out *wad.Wad, m *MapInfo) error {
	var err error

	m.lev, err = in.ReadLevel(m.idx)
	if err != nil {
		return err
	}

	fmt.Printf("%10s\b\b\b\b\b\b\b\b\b\b", m.name)

	ClearIds()

	if m.IsBoss() {
		// boss maps are special, leave 'em alone
	} else {
		m.AnalyseMap()

		m.Weaponize()
		m.Monsterize()
		m.Itemize()

		m.RebuildThings()
	}

	if err = out.WriteLevel(m.lev); err != nil {
		return err
	}

	// allow garbage collection
	m.lev = nil

	return Ok
}

func (m *MapInfo) RebuildThings() {
	m.lev.Things = make([]*wad.Thing, 0, len(m.things))

	for _, athing := range m.things {
		if !athing.discard {
			m.lev.Things = append(m.lev.Things, athing.raw)
		}
	}
}

func (m *MapInfo) AddThing(x, y int, kind int, flags wad.ThingFlags) *AnThing {
	th := new(wad.Thing)

	th.X = int16(x)
	th.Y = int16(y)
	th.Kind = uint16(kind)
	th.Flags = flags

	// place new things at end of the THINGS lump
	th.Index = 999999

	athing := new(AnThing)

	athing.raw = th

	// leave sector as nil.
	// [ it could be computed but there is no need ]

	m.things = append(m.things, athing)

	return athing
}

func CreateInfoLump(out *wad.Wad) {
	var sb strings.Builder

	fmt.Fprintf(&sb, "THE POPULATOR (version %s)\r\n", VERSION)
	fmt.Fprintf(&sb, "os = %s/%s\r\n", runtime.GOOS, runtime.GOARCH)
	fmt.Fprintf(&sb, "seed = %d\r\n", Options.seed)

	// terminate lump with ^Z and a NUL character
	sb.WriteByte(26)
	sb.WriteByte(0)

	str := sb.String()

	out.WriteLump("POPULDAT", []byte(str))
}

//------------------------------------------------------------------------

// PointToSector determines the sector containing the given point.
// returns NIL if point is not inside any sector.
func PointToSector(lev *wad.Level, px, py int) *wad.Sector {
	// offset the point by a small amount, to prevent the
	// line-casting functions from hitting a vertex
	rx := float64(px) + 0.31415
	ry := float64(py) + 0.25

	// look in four directions: N, E, S, W.
	// generally they will all agree, but there are rare places where
	// the map geometry is broken and we still want a "good" answer.
	var scanned [4]*wad.Sector

	for i := 0; i < 4; i++ {
		dir := Direction(i)

		ld, side := ClosestLine(lev, rx, ry, dir)

		scanned[i] = nil

		if ld != nil {
			if side > 0 && ld.Front != nil {
				scanned[i] = ld.Front.Sector
			}
			if side < 0 && ld.Back != nil {
				scanned[i] = ld.Back.Sector
			}
		}
	}

	/* DEBUG CODE
	if scanned[1] != scanned[0] ||
		scanned[2] != scanned[0] ||
		scanned[3] != scanned[0] {
		fmt.Fprintf(os.Stdout, "THING #%d MISMATCHED: %p %p %p %p\n", curThing,
		scanned[0], scanned[1], scanned[2], scanned[3])
	}
	*/

	// if two or more directions agree, choose that
	for i := 0; i < 3; i++ {
		for k := i + 1; k < 4; k++ {
			if scanned[i] == scanned[k] {
				return scanned[i]
			}
		}
	}

	// map geometry must be quite broken here, so what we return is
	// rather arbitrary.

	for i := 0; i < 4; i++ {
		if scanned[i] != nil {
			return scanned[i]
		}
	}

	return scanned[0]
}

func ClosestLine(lev *wad.Level, rx, ry float64, dir Direction) (res *wad.LineDef, side int) {
	var best_dist float64 = 9e9

	for _, ld := range lev.Lines {
		// ignore lines with same sector on both sides
		// [ to handle the "self-referencing" linedefs used by some old
		//   mapping tricks to create deep water and force fields ]
		if ld.Front != nil && ld.Back != nil && ld.Front.Sector == ld.Back.Sector {
			continue
		}

		ly1 := float64(ld.Start.Y)
		lx1 := float64(ld.Start.X)

		ly2 := float64(ld.End.Y)
		lx2 := float64(ld.End.X)

		if dir.IsHoriz() {
			// does the linedef cross the horizontal ray?
			if (ly1 >= ry && ly2 >= ry) || (ly1 <= ry && ly2 <= ry) {
				continue
			}

			dist := lx1 + (ry-ly1)*(lx2-lx1)/(ly2-ly1) - rx

			if dir == LF {
				dist = -dist
			}

			if dist > 0 && dist < best_dist {
				best_dist = dist
				res = ld

				if (ly1 > ly2) == (dir == RT) {
					side = +1 // front
				} else {
					side = -1 // back
				}
			}

		} else { // dir.IsVert()

			// does the linedef cross the vertical ray?
			if (lx1 >= rx && lx2 >= rx) || (lx1 <= rx && lx2 <= rx) {
				continue
			}

			dist := ly1 + (rx-lx1)*(ly2-ly1)/(lx2-lx1) - ry

			if dir == DN {
				dist = -dist
			}

			if dist > 0 && dist < best_dist {
				best_dist = dist
				res = ld

				if (lx1 > lx2) == (dir == DN) {
					side = +1 // front
				} else {
					side = -1 // back
				}
			}
		}
	}

	return
}

func CanCrossLine(ld *wad.LineDef, monster bool) bool {
	if ld.Front == nil || ld.Back == nil {
		return false
	}

	/* check blocking flags */

	if (ld.Flags & wad.LF_Blocking) != 0 {
		return false
	}
	if monster && (ld.Flags&wad.LF_BlockMonsters) != 0 {
		return false
	}
	// NOTE: the next two are ZDoom specific
	if (ld.Flags & wad.ZLF_BlockAll) != 0 {
		return false
	}
	if !monster && (ld.Flags&wad.ZLF_BlockPlayers) != 0 {
		return false
	}

	/* check player fits on XY plane */

	dx := Abs(int(ld.Start.X) - int(ld.End.X))
	dy := Abs(int(ld.Start.Y) - int(ld.End.Y))

	if dx < 36 && dy < 36 {
		return false
	}

	/* check heights */

	from := ld.Front.Sector
	dest := ld.Back.Sector

	f1, c1 := int(from.FloorH), int(from.CeilH)
	f2, c2 := int(dest.FloorH), int(dest.CeilH)

	// too big a step?
	// [ we purposely disallow drop-offs here ]
	// FUTURE : support jumping for Hexen
	if Abs(f1-f2) > 24 {
		return false
	}

	// vertical gap too small?
	if Min(c1, c2)-Max(f1, f2) < MIN_HEIGHT {
		return false
	}

	return true
}

func CanCrossLine_IgnoreFloor(ld *wad.LineDef) bool {
	if ld.Front == nil || ld.Back == nil {
		return false
	}

	/* check blocking flags */

	if (ld.Flags & wad.LF_Blocking) != 0 {
		return false
	}

	// NOTE: the next two are ZDoom specific
	if (ld.Flags & wad.ZLF_BlockAll) != 0 {
		return false
	}
	if (ld.Flags&wad.ZLF_BlockPlayers) != 0 {
		return false
	}

	/* check player fits on XY plane */

	dx := Abs(int(ld.Start.X) - int(ld.End.X))
	dy := Abs(int(ld.Start.Y) - int(ld.End.Y))

	if dx < 36 && dy < 36 {
		return false
	}

	/* check headroom */

	from := ld.Front.Sector
	dest := ld.Back.Sector

	f1, c1 := int(from.FloorH), int(from.CeilH)
	f2, c2 := int(dest.FloorH), int(dest.CeilH)

	if Min(c1, c2)-Max(f1, f2) < MIN_HEIGHT {
		return false
	}

	return true
}

// LineSwitchSector determines the sector where the player would
// normally press the switch (on front of the given line).  We
// try to detect switch niches that are common in DOOM maps
// (such as the MAP02 exit switch in DOOM 2).
func LineSwitchSector(lev *wad.Level, line *wad.LineDef) *wad.Sector {
	// mid-point of line
	mx := (float64(line.Start.X) + float64(line.End.X)) / 2
	my := (float64(line.Start.Y) + float64(line.End.Y)) / 2

	// create a normal vector
	dx := float64(line.End.Y) - float64(line.Start.Y)
	dy := float64(line.Start.X) - float64(line.End.X)

	dlen := math.Hypot(dx, dy)

	if dlen > 1 {
		dx /= dlen
		dy /= dlen
	}

	// push some distance out.  This is not quite the full USERANGE
	// because I want to avoid going past a steep cliff.
	const use_dist = 37

	px := int(mx + dx * use_dist)
	py := int(my + dy * use_dist)

	/// fmt.Printf("Line %d : (%1.0f %1.0f) delta (%1.2f %1.2f)\n", line.Index, mx, my, dx, dy)

	return PointToSector(lev, px, py)
}
