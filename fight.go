// Copyright 2019 Andrew Apted.
// This code is under the GNU General Public License, version 3
// or (at your option) any later version.

/*

===================
 BATTLE SIMULATION
===================

Input
-----

monsters : list of monsters that the player must kill

weapons : list of weapons that player can use


Output
------

stats : health that player needs to survive the battle +
        ammo quantities required by the player


NOTES
-----

*  Results are all >= 0 and can be partial (like 3.62 rockets).

*  Health result is stored in the 'stats' table (as "HEALTH").

*  Monsters are "fought" one by one in the given list, sorted
   from the strongest to the weakest (an order similar to how the
   player would tackle them).

   Your weapons can damage other monsters though, via such things
   as rocket splash, BFG spray, and shotgun spread.

*  Weapons are "fired" in short rounds.  Each round a weapon is
   chosen based on their intrinsic 'pref' value (and modified by a
   'factor' value if present), as well as other things like the
   'weap_prefs' of the current monster.  The weapon's damage is
   used to decrease the monster's health, recording the ammo usage.
   Dead monsters get removed from the list.

*  Armor is not modelled here.  Instead you can assume that some
   percentage of the returned "health" would have been saved if
   the player was wearing armor.

*  Powerups like Invulnerability or Berserk are not modelled.
   Invulnerability could be handled by detecting which monsters
   (often bosses) to be fought and then simply omitting them
   from the simulation.  Other powerups are considered to be
   bonuses for the player.

*  Infighting between monsters is modelled via 'infight_damage'
   field of each monster.  Those values were determined from
   demo analysis and represent an average amount of damage which
   each monster of that kind inflicts on other monsters.

*/

package main

import "fmt"
import "sort"
import "strings"
import "math/rand"

type Stats struct {
	values []StatValue
}

type StatValue struct {
	k string
	v float64
}

type FightState struct {
	monsters []*FightMon
	weapons  []*Weapon
	stats    *Stats
}

type FightMon struct {
	def    *MonsterDef
	health float64
	order  float64
}

const DEFAULT_INFIGHT_DAMAGE = 20

func FightSimulator(monsters []*Bunch, weapons []*Weapon, stats *Stats) {
	var fs FightState

	if len(weapons) == 0 {
		panic("FightSimulator with no weapons")
	}

	fs.monsters = make([]*FightMon, 0)
	fs.weapons = weapons
	fs.stats = stats

	for _, bunch := range monsters {
		for i := 0; i < bunch.count; i++ {
			M := new(FightMon)

			M.def = bunch.def
			M.health = bunch.def.Health
			M.order = bunch.def.Health + rand.Float64()

			fs.monsters = append(fs.monsters, M)
		}
	}

	// put toughest monster first, weakest last.
	sort.Slice(fs.monsters, func(i, k int) bool {
		return fs.monsters[i].order > fs.monsters[k].order
	})

	// compute health needed by player
	for _, M := range fs.monsters {
		fs.stats.AddTo("HEALTH", M.def.Damage)
	}

	// simulate infighting
	// [ done *after* computing player health, as the damage values are based
	//   on demo analysis and implicitly contain an infighing factor ]
	for _, M := range fs.monsters {
		fs.MonsterInfight(M)
	}

	// run simulation until all monsters are dead
	for len(fs.monsters) > 0 {
		W := fs.SelectWeapon()

		fs.PlayerShoot(W)

		// guard against infinite loop: always take a tiny bit of health
		// off the first monster
		fs.monsters[0].health -= 0.01

		fs.RemoveDeadMon()
	}

	fs.FixupHexenMana()
}

func (fs *FightState) RemoveDeadMon() {
	for i := len(fs.monsters) - 1; i >= 0; i-- {
		if fs.monsters[i].health <= 0 {
			copy(fs.monsters[i:], fs.monsters[i+1:])
			fs.monsters = fs.monsters[0 : len(fs.monsters)-1]
		}
	}
}

func (fs *FightState) SelectWeapon() *Weapon {
	///	first_mon := fs.monsters[1]

	// determine probability for each weapon
	prob_tab := make([]float64, len(fs.weapons))

	total := 0

	for idx, W := range fs.weapons {
		prob := W.def.Usage

		prob *= W.usage_factor

		if prob > 0 {
			prob_tab[idx] = prob
			total++
		}
	}

	// this can only happen if config files are messed up
	if total == 0 {
		return fs.weapons[0]
	}

	index := IndexByProbs(prob_tab)

	return fs.weapons[index]
}

func (fs *FightState) HurtMon(idx int, W *Weapon, damage float64) {
	if idx >= len(fs.monsters) {
		return
	}

	M := fs.monsters[idx]

	M.health -= damage
}

func (fs *FightState) PlayerShoot(W *Weapon) {
	fs.HurtMon(0, W, W.def.Damage)

	// simulate splash damage | shotgun spread
	for i, splash := range W.def.Splash {
		fs.HurtMon(1+i, W, splash)
	}

	per := W.def.Ammo_per_shot
	if per > 0 {
		fs.stats.AddTo(W.def.Ammo_type, per)
	}
}

func (fs *FightState) CanInfight(info1 *MonsterDef, info2 *MonsterDef) bool {
	// returns true if the first monster can hurt the second

	species1 := info1.Species
	species2 := info2.Species

	if species1 == "" {
		species1 = info1.name
	}
	if species2 == "" {
		species2 = info2.name
	}

	if species1 == species2 {
		return info1.Disloyal
	}

	return true
}

func (fs *FightState) MonsterInfight(M *FightMon) {
	// Note: we don't check if monsters "die" here, not needed

	// collect all other monsters which can be fought
	others := make([]*FightMon, 0)

	var total_weight float64

	for _, P := range fs.monsters {
		if P != M && fs.CanInfight(M.def, P.def) {
			others = append(others, P)
			total_weight += P.def.Health
		}
	}

	// nothing else to fight with?
	if len(others) == 0 || total_weight <= 0 {
		return
	}

	// distribute the 'infight_damage' value
	damage := M.def.Infight_damage
	if damage == 0 {
		damage = DEFAULT_INFIGHT_DAMAGE
	}

	// bump up the damage (higher than demo analysis, but seems necessary)
	damage = damage * 1.5

	for _, P := range others {
		// damage is weighted, bigger monsters get a bigger share
		factor := P.def.Health / total_weight

		P.health -= damage * factor
	}
}

func (fs *FightState) FixupHexenMana() {
	dual := fs.stats.Get("DUAL_MANA")

	if dual > 0 {
		fs.stats.AddTo("BLUE_MANA", dual)
		fs.stats.AddTo("GREEN_MANA", dual)
		fs.stats.Set("DUAL_MANA", 0)
	}
}

//----------------------------------------------------------------------

func NewStats() Stats {
	var st Stats
	st.values = make([]StatValue, 0)
	return st
}

func (st *Stats) Get(k string) float64 {
	for i := 0; i < len(st.values); i++ {
		if st.values[i].k == k {
			return st.values[i].v
		}
	}
	return 0
}

func (st *Stats) Set(k string, v float64) {
	for i := 0; i < len(st.values); i++ {
		if st.values[i].k == k {
			st.values[i].v = v
			return
		}
	}

	st.values = append(st.values, StatValue{k, v})
}

func (st *Stats) AddTo(k string, v float64) {
	for i := 0; i < len(st.values); i++ {
		if st.values[i].k == k {
			st.values[i].v += v
			return
		}
	}

	st.values = append(st.values, StatValue{k, v})
}

func (st *Stats) String() string {
	var sb strings.Builder

	sb.WriteString("{")

	for i := 0; i < len(st.values); i++ {
		if i > 0 {
			sb.WriteString(", ")
		}
		fmt.Fprintf(&sb, "%s:%1.1f", st.values[i].k, st.values[i].v)
	}

	sb.WriteString("}")

	return sb.String()
}
