module gitlab.com/andwj/populate

go 1.17

require (
	gitlab.com/andwj/argv v0.0.0-20171228033253-fb72efa6c68c
	gitlab.com/andwj/ini v0.0.0-20180106134907-4577cc9cf1de
	gitlab.com/andwj/wad v0.0.0-20180916105448-06ca822f8d4f
)
