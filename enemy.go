// Copyright 2019 Andrew Apted.
// This code is under the GNU General Public License, version 3
// or (at your option) any later version.

/*

Enemy selection and placement

*/

package main

import "fmt"
import "math"
import "math/rand"
import "sort"

import "gitlab.com/andwj/wad"

type Hostility struct {
	all_bunches []*Bunch

	skipped_mon map[string]bool
	reduced_mon map[string]bool
}

// Bunch is a small group of monsters of the same type which
// are fairly close together.  We replace a whole bunch with
// another bunch and adjust the numbers, e.g. four imps might
// become a single mancubus, or vice versa.
type Bunch struct {
	def   *MonsterDef
	count int

	cluster *Cluster // cluster it is in, NEVER nil

	// the center point of the bunch
	mid_x int
	mid_y int

	angle int
}

func (m *MapInfo) Monsterize() {
	m.all_bunches = make([]*Bunch, 0)

	m.DecideSkipped()

	m.FilterMonsters()
	m.CollectSpots()
	m.ReplaceMonsters()
}

func (m *MapInfo) DecideSkipped() {
	m.skipped_mon = make(map[string]bool)
	m.reduced_mon = make(map[string]bool)

	for _, def := range Game.monsters {
		if def.Skip_prob > 0 && Odds(def.Skip_prob) {
			m.skipped_mon[def.name] = true
			m.reduced_mon[def.name] = Odds(15)
		}
	}
}

func (m *MapInfo) FilterMonsters() {
	for _, athing := range m.things {
		if athing.def == nil || athing.def.kind != ENT_Monster {
			continue
		}

		def := athing.def.monster
		_ = def

		// by default, filter out all monsters
		athing.discard = true

		// skip monsters not in single player
		if (athing.raw.Flags & wad.DTF_Not_SP) != 0 {
			continue
		}

		// ignore all skills except for UV
		if (athing.raw.Flags & wad.TF_Hard) == 0 {
			continue
		}

		// in a "weird" place?
		var cl *Cluster

		if athing.sector != nil &&
			athing.sector.area != nil &&
			athing.sector.area.cluster != nil {

			cl = athing.sector.area.cluster
		}

		if cl == nil {
			continue
		}

		// no fighting in voodoo closets
		if cl.kind == "VOODOO" {
			continue
		}

		/* Ok */

		m.StoreMonster(athing, cl)
	}
}

func (m *MapInfo) StoreMonster(athing *AnThing, cl *Cluster) {
	def := athing.def.monster

	x := int(athing.raw.X)
	y := int(athing.raw.Y)

	// look for an existing bunch in same cluster
	for _, b := range cl.bunches {
		if b.def == def && b.count < 6 {
			dx := Abs(x - b.mid_x)
			dy := Abs(y - b.mid_y)

			if Max(dx, dy) < 400 {
				b.AddMon(x, y)
				return
			}
		}
	}

	// create a new bunch
	bunch := NewBunch(def, cl, x, y)

	bunch.angle = int(athing.raw.Angle)

	cl.bunches = append(cl.bunches, bunch)
	m.all_bunches = append(m.all_bunches, bunch)
}

func NewBunch(def *MonsterDef, cl *Cluster, x, y int) *Bunch {
	bunch := new(Bunch)
	bunch.def = def
	bunch.count = 1
	bunch.cluster = cl
	bunch.mid_x = x
	bunch.mid_y = y

	return bunch
}

func (bunch *Bunch) AddMon(x, y int) {
	bunch.count += 1

	// update mid point
	dx := x - bunch.mid_x
	dy := y - bunch.mid_y

	bunch.mid_x += dx / bunch.count
	bunch.mid_y += dy / bunch.count
}

func (bunch *Bunch) String() string {
	return fmt.Sprintf("[%d x %s]", bunch.count, bunch.def.name)
}

func (m *MapInfo) CollectSpots() {
	for _, cl := range m.clusters {
		if cl.kind == "VOODOO" {
			continue
		}

		for _, area := range cl.areas {
			// this usually only fails when area is too small
			if !m.SpotRender(area) {
				continue
			}

			// the order here matters
			grid.FindItemSpots(&cl.item_spots, area)
			grid.FindMonsterSpots(&cl.mon_spots, area)

			// shuffle into a random order
			cl.mon_spots.Shuffle()
			cl.item_spots.Shuffle()
		}
		/*
			fmt.Printf("%s : %d item spots, %d mon spots\n", cl.name, len(cl.item_spots), len(cl.mon_spots))
		*/
	}
}

func (m *MapInfo) ReplaceMonsters() {
	for _, bunch := range m.all_bunches {
		m.SelectMonster(bunch, false)
	}

	// we need to process the largest monsters first, otherwise
	// fragmentation of the spot lists means that huge monsters
	// may fail to be added.

	sort.SliceStable(m.all_bunches, func(i, k int) bool {
		b1 := m.all_bunches[i]
		b2 := m.all_bunches[k]

		return b1.def.R > b2.def.R
	})

	for _, bunch := range m.all_bunches {
		if bunch.count > 0 {
			m.PlaceMonsterBunch(bunch)
		}
	}
}

func (m *MapInfo) PlaceMonsterBunch(bunch *Bunch) {
	var last *Spot

	for i := 0; i < bunch.count; i++ {
		spot := m.PickSpotForMonster(bunch, last)

		// if no spot can be found, pick another monster type
		// (unless partway through a bunch...)
		if spot == nil {
			if i > 0 {
				// truncate current bunch and abort
				bunch.count = i
				return
			}

			for loop := 0; spot == nil && loop < 5; loop++ {
				// pick another, smaller type
				m.SelectMonster(bunch, true)
				if bunch.count == 0 {
					return
				}
				spot = m.PickSpotForMonster(bunch, last)
			}

			if spot == nil {
				bunch.count = 0
				return
			}
		}

		flags := wad.TF_Easy | wad.TF_Medium | wad.TF_Hard

		if Odds(33) {
			flags |= wad.TF_Ambush
		}

		athing := m.AddThing(spot.x, spot.y, bunch.def.Type, flags)
		athing.raw.Angle = int16(bunch.angle)
	}
}

func (m *MapInfo) SelectMonster(bunch *Bunch, smaller bool) {
	prob_tab := make([]float64, len(Game.monsters))

	total := 0

	for idx, def := range Game.monsters {
		prob := def.Prob

		// too early in the mapset for this monster?
		if def.Appear > m.mon_along + 2.5 {
			prob = 0
		} else if def.Appear > m.mon_along {
			prob /= 10
		}

		if m.skipped_mon[def.name] {
			prob = 0
		} else if m.reduced_mon[def.name] {
			prob /= 2.5
		}

		// want a smaller monster?
		if smaller && def.R >= bunch.def.R {
			prob = 0
		}

		// don't put melee monsters on ledges or in cages
		if bunch.cluster.kind == "CLOSET" && def.Melee {
			prob = 0
		}

		prob_tab[idx] = prob

		if prob > 0 {
			total++
		}
	}

	// when not wanting a smaller monster, this can only happen if
	// the configs are messed up
	if total == 0 {
		bunch.count = 0
		return
	}

	/// old_str := bunch.String()

	idx := IndexByProbs(prob_tab)
	def := Game.monsters[idx]

	old_def := bunch.def
	bunch.def = def

	if !smaller {
		// attempt to conserve the overall "toughness" of the bunch,
		// where a group of weak monsters becomes a single strong
		// one, *and* vice versa.

		ratio := math.Max(1, old_def.Toughness) / math.Max(1, def.Toughness)

		if ratio < 0.2 {
			ratio = 0.2
		} else if ratio > 5.0 {
			ratio = 5.0
		}

		bunch.count = int(float64(bunch.count) * ratio)

		if bunch.count < 1 {
			bunch.count = 1
		}

		/*
			fmt.Printf("%s -> %s      \n", old_str, bunch.String())
		*/
	}
}

func (m *MapInfo) PickSpotForMonster(bunch *Bunch, last *Spot) *Spot {
	// WISH: collect non-locked neighbor clusters and try them
	//       when the main cluster fails.

	for pass := 0; pass < 1; pass++ {
		cl := bunch.cluster

		sp := m.PickMatchingSpot(bunch, cl, last)
		if sp != nil {
			return sp
		}
	}

	// failed
	return nil
}

func (m *MapInfo) PickMatchingSpot(bunch *Bunch, cl *Cluster, last *Spot) *Spot {
	var best *Spot
	var best_dist float64 = 9e9

	for _, sp := range cl.mon_spots.spots {
		if sp.used {
			continue
		}

		// can the monster fit?
		if bunch.def.R > float64(sp.r) || bunch.def.H >= float64(sp.h) {
			continue
		}

		dx := float64(bunch.mid_x) - float64(sp.x)
		dy := float64(bunch.mid_y) - float64(sp.y)

		if last != nil {
			dx = float64(last.x) - float64(sp.x)
			dy = float64(last.y) - float64(sp.y)
		}

		dist := math.Hypot(dx, dy)

		if last == nil {
			// quantize distance to very coarse granularity.
			// this will randomize the location of the new monster.
			dist = math.Floor(dist / 400)
		}

		// tie breaker
		dist += rand.Float64()

		if dist < best_dist {
			best = sp
			best_dist = dist
		}
	}

	if best != nil {
		// don't waste really big spots on small monsters
		best = cl.mon_spots.SplitSpot(best, int(bunch.def.R))

		// mark it as used
		best.used = true
	}

	return best
}
